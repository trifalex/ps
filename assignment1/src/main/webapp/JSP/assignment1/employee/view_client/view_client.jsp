<%--
  Created by IntelliJ IDEA.
  User: Alexandru
  Date: 07.11.2016
  Time: 19:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="http://getbootstrap.com/examples/sticky-footer-navbar/sticky-footer-navbar.css">
    <link rel="stylesheet" type="text/css" href="<c:url value="${pageContext.request.contextPath}/style/bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="${pageContext.request.contextPath}/style/bootstrap/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="${pageContext.request.contextPath}/style/CSS/Guest/guest_style.css" />">

    <script src="<c:url value="${pageContext.request.contextPath}/style/JS/jquery-3.1.1.min.js" />"></script>
    <script src="<c:url value="${pageContext.request.contextPath}/style/bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js" />"></script>
    <title>View Client</title>
</head>
<body>

<div class="guest-wrapper">
    <div class="guest-wrapper-page">
        <jsp:include page="${pageContext.request.contextPath}/JSP/common/employee/employee_header.jsp"/>
        <div class="guest-body">
            <div class="register-login-form">
                <form class="form-horizontal" method="post" action="${pageContext.request.contextPath}/assignment1/employee/client/update/${client.idClient}">
                    <div class="form-group">
                        <label class="col-sm-1 control-label">First name</label>
                        <div class="col-sm-3">
                            <input type="text" readonly class="form-control" placeholder="First name" name="firstName" value="${client.firstName}">
                        </div>
                        <label class="col-sm-8 error-style">${firstName}</label>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-1 control-label">Last name</label>
                    <div class="col-sm-3">
                        <input type="text" readonly class="form-control" placeholder="Last name" name="lastName" value="${client.lastName}">
                    </div>
                    <label class="col-sm-8 error-style">${lastName}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Card Number</label>
                        <div class="col-sm-3">
                            <input type="text" readonly class="form-control" placeholder="Card Number" name="cardNumber" value="${client.cardNumber}">
                        </div>
                        <label class="col-sm-8 error-style">${cardNumber}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Address</label>
                        <div class="col-sm-3">
                            <input type="text" readonly class="form-control" placeholder="Address" name="address" value="${client.address}">
                        </div>
                        <label class="col-sm-8 error-style">${address}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Personal Number Code</label>
                        <div class="col-sm-3">
                            <input type="text" readonly class="form-control" placeholder="Personal Number Code" name="personalNumberCode" value="${client.personalNumberCode}">
                        </div>
                        <label class="col-sm-8 error-style">${personalNumberCode}</label>
                    </div>
                </form>
            </div>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>Account ID</th>
                <th>Account Number</th>
                <th>Account Full Name</th>
                <th>Account CVV</th>
                <th>Balance</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <c:set var="contor" value="1"/>
            <c:forEach items="${accounts}" var="account">
                <tr>
                    <th scope="row">${contor}</th>
                    <td>${account.idAccount}</td>
                    <td>${account.accountNumber}</td>
                    <td>${account.accountFullName}</td>
                    <td>${account.accountCVV}</td>
                    <td>${account.balance}</td>
                    <td>
                        <form method="get" id="updateAccountForm${contor}"
                              action="${pageContext.request.contextPath}/assignment1/employee/client/update-account/${account.idAccount}"></form>
                        <a class="no-hover" href="#"
                           onclick="document.getElementById('updateAccountForm${contor}').submit();">Update</a>
                    </td>
                    <td>
                        <a class="no-hover" href="#" data-toggle="modal" data-target="#deleteModal${contor}">Delete</a>
                        <!-- modal for deleting client-->
                        <div class="modal fade" tabindex="-1"
                             id="deleteModal${contor}" role="dialog"
                             aria-labelledby="deleteModal${contor}">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h4 class="modal-title"
                                            id="modalLabel${contor}">
                                            <span class="black-color">Delete ${account.accountNumber}</span>
                                        </h4>
                                    </div>
                                    <div class="modal-body">
                                    <span class="primary-color">Are you sure you want
                                        to delete ${account.accountFullName} ?</span>
                                    </div>
                                    <div class="modal-footer">
                                        <form method="post" class="form-horizontal" id="deleteAccountForm${contor}"
                                              action="${pageContext.request.contextPath}/assignment1/employee/client/delete-account/${account.idAccount}">
                                            <button type="button" class="btn btn-danger"
                                                    data-dismiss="modal">Close
                                            </button>
                                            <button type="submit" class="btn btn-primary">Delete Account</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <c:set var="contor" value="${contor + 1}"/>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <jsp:include page="${pageContext.request.contextPath}/JSP/common/employee/employee_footer.jsp"/>
</div>
</body>
</html>
