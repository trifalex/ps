package edu.utcluj.ro.assignment1.controller.employee;

import edu.utcluj.ro.assignment1.exceptions.DatabaseException;
import edu.utcluj.ro.assignment1.model.Account;
import edu.utcluj.ro.assignment1.model.Client;
import edu.utcluj.ro.assignment1.util.ServiceUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Alexandru Trif on 19.04.2017.
 */
@WebServlet("/assignment1/employee/client/view/*")
public class ViewClientController extends HttpServlet {

    private static final String VIEW_EMPLOYEE_VIEW_CLIENT_PATH = "../../../../JSP/assignment1/employee/view_client/view_client.jsp";
    private static final String VIEW_404_PATH = "../../../../JSP/assignment1/employee/error/404.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String[] pathInfo = req.getPathInfo().split("/");
        if(pathInfo.length == 2) {

            String clientIdToUpdate = pathInfo[1];

            try {

                int idClient = Integer.valueOf(clientIdToUpdate);
                Client client = ServiceUtil.getClientService().getClientById(idClient);

                if (client != null) {

                    List<Account> accounts = ServiceUtil.getAccountService().getAllAccountsByClientId(idClient);
                    req.setAttribute("client", client);
                    req.setAttribute("accounts", accounts);
                    req.getRequestDispatcher(VIEW_EMPLOYEE_VIEW_CLIENT_PATH).forward(req, resp);
                } else {
                    req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
                }
            } catch(NumberFormatException e) {
                req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
            } catch (DatabaseException e) {
                e.printStackTrace();
                req.setAttribute("generalError", e.getMessage());
                req.getRequestDispatcher(VIEW_EMPLOYEE_VIEW_CLIENT_PATH).forward(req, resp);
            }
        } else {
            req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
        }
    }
}
