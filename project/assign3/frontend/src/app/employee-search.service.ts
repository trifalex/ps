import {Injectable} from "@angular/core";
import {Headers, Http} from "@angular/http";

import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/map";

import {Employee} from "./employee";
import {AuthenticationService} from "./authentication.service";

@Injectable()
export class EmployeeSearchService {

  private headers = new Headers({
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + this.authenticationService.getToken()
  });

  constructor(private http: Http,
              private authenticationService: AuthenticationService) {
  }

  search(term: string): Observable<Employee[]> {
    return this.http
      .get(`http://localhost:8080/api/employees/search/findByName?name=${term}`, {headers: this.headers})
      .map(response => response.json()._embedded.employees as Employee[]);
  }
}
