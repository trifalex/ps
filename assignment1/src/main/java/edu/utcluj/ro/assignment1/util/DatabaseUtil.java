package edu.utcluj.ro.assignment1.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by Alexandru Trif on 11.04.2017.
 */
public class DatabaseUtil {

    private static Connection connection = null;

    public static Connection getConnection () {

        if (connection != null) {
            return connection;
        } else {

            DatabaseUtil databaseUtil = new DatabaseUtil();

            Properties properties = databaseUtil.getDatabaseProperties();
            if (properties != null) {

                String driver = properties.getProperty("driver");
                String url = properties.getProperty("url");
                String user = properties.getProperty("user");
                String password = properties.getProperty("password");
                try {
                    Class.forName(driver);
                    connection = DriverManager.getConnection(url, user, password);
                    return connection;
                } catch (ClassNotFoundException  | SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            } else {
                return null;
            }
        }
    }
    private Properties getDatabaseProperties() {

        Properties properties = null;
        InputStream inputStream = null;
        try {
            inputStream = getClass().getResourceAsStream("/Db/db.properties");
            properties = new Properties();
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return properties;
    }
}
