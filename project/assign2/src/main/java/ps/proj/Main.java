package ps.proj;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import ps.proj.model.Company;
import ps.proj.model.Employee;
import ps.proj.model.Project;
import ps.proj.service.EmployeeService;
import ps.proj.service.ProjectService;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by atrif on 5/7/2017.
 */
public class Main {

    public static void main(String[] args) throws IOException {

        Company company = new Company();

        Employee employee = new Employee();
        employee.setIdEmployee(1);
        employee.setAddress("Plopilor");
        employee.setBirthDate(new Date());
        employee.setFirstName("Alex");
        employee.setLastName("Alex");

        Project project = new Project();
        project.setClientName("adidas");
        project.setCost(200.00);
        project.setIdProject(3);
        project.setStartDate(new Date());

        List<Project> projects = new ArrayList<>();
        projects.add(project);

//        employee.setProjects(projects);

//        List<Employee> employees = new ArrayList<>();
//        employees.add(employee);
//        company.setEmployees(employees);
//
//
//        company.setName("Catalysts");
//        company.setRegistryDate(new Date());


        ObjectMapper mapper = new ObjectMapper();

//        FileWriter writer = new FileWriter(new File("company.json"));

//        mapper.writeValue(writer, company);

        Company company1 = mapper.readValue(new FileReader(new File("company.json")), Company.class);
//
//        Employee searchEmployee = new EmployeeService().getEmployeeById(123);
//        EmployeeService employeeService = new EmployeeService();
////        employeeService.createEmployee(employee);
//        employeeService.addProjectToEmployee(employee, project);

//        System.out.println(searchEmployee.getFirstName() + " " + searchEmployee.getLastName());
        company1.setContracts(projects);
        FileWriter writer = new FileWriter(new File("company.json"));

        mapper.writeValue(writer, company1);
//        ProjectService projectService = new ProjectService();
//        projectService.createProject(project);

    }
}
