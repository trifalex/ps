package edu.utcluj.ro.assignment1.controller.guest;

import edu.utcluj.ro.assignment1.exceptions.DatabaseException;
import edu.utcluj.ro.assignment1.model.User;
import edu.utcluj.ro.assignment1.service.BCryptHashingService;
import edu.utcluj.ro.assignment1.util.ServiceUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Alexandru Trif on 11.04.2017.
 */
public class LoginController extends HttpServlet {

    private static final String VIEW_GUEST_REGISTER_PATH = "../../../JSP/assignment1/guest/login/login.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.getRequestDispatcher(VIEW_GUEST_REGISTER_PATH).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            User userToLogin = ServiceUtil.getUserService().getUserByEmail(req.getParameter("email"));
            //if there is a user with the inserted email, check if passwords match
            if (userToLogin != null) {
                String insertedPassword = req.getParameter("password");
                if(BCryptHashingService.checkIfPasswordMatchHashcode(insertedPassword, userToLogin.getPassword())) {
                    HttpSession httpSession = req.getSession();
                    //setting session to expiry in 20 minutes
                    httpSession.setMaxInactiveInterval(20 * 60);
                    httpSession.setAttribute("userLoggedIn", userToLogin);

                    if(userToLogin.getRole().equals("admin")) {
                        resp.sendRedirect("/assignment1/admin/home/");
                    } else if(userToLogin.getRole().equals("employee")) {
                        resp.sendRedirect("/assignment1/employee/home/");
                    } else {
                        httpSession.invalidate();
                        resp.sendRedirect("/assignment1/guest/login/");
                    }
                } else {
                    req.setAttribute("authenticationError", "Invalid password or email!");
                    req.getRequestDispatcher(VIEW_GUEST_REGISTER_PATH).forward(req, resp);
                }
            } else {
                req.setAttribute("authenticationError", "Invalid password or email!");
                req.getRequestDispatcher(VIEW_GUEST_REGISTER_PATH).forward(req, resp);
            }
        } catch (DatabaseException e) {
            req.setAttribute("generalError", e.getMessage());
            req.getRequestDispatcher(VIEW_GUEST_REGISTER_PATH).forward(req, resp);
        }
    }
}
