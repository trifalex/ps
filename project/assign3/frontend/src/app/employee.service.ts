import {Injectable} from "@angular/core";

import {Employee} from "./employee";
import {Headers, Http} from "@angular/http";

import "rxjs/add/operator/toPromise";
import {AuthenticationService} from "./authentication.service";

@Injectable()
export class EmployeeService {

  private employeesUrl = 'http://localhost:8080/api/employees';
  private headers = new Headers({
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + this.authenticationService.getToken()
  });

  constructor(private http: Http,
              private authenticationService: AuthenticationService) {
  }

  getEmployees(): Promise<Employee[]> {
    return this.http
      .get(this.employeesUrl, {headers: this.headers})
      .toPromise()
      .then(response => response.json()._embedded.employees as Employee[])
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  // getEmployee(id: number): Promise<Employee> {
  //   const url = `${this.employeesUrl}/${id}`;
  //   return this.http.get(url)
  //     .toPromise()
  //     .then(response => response.json() as Employee)
  //     .catch(this.handleError);
  // }
  getEmployee(id: number): Promise<Employee> {
    return this.getEmployees()
      .then(employees => employees.find(employee => employee.id === id))
  }

  delete(id: number): Promise<void> {
    console.log(`hero.service - deleting ${id}`);
    const url = `${this.employeesUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(name: string): Promise<Employee> {
    return this.http
      .post(this.employeesUrl, JSON.stringify({name: name}), {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Employee)
      .catch(this.handleError);
  }

  update(employee: Employee): Promise<Employee> {
    const url = `${this.employeesUrl}/${employee.id}`;
    return this.http
      .put(url, JSON.stringify(employee), {headers: this.headers})
      .toPromise()
      .then(() => employee)
      .catch(this.handleError);
  }
}
