package edu.utcluj.ro.assignment1.util;

import edu.utcluj.ro.assignment1.service.AccountService;
import edu.utcluj.ro.assignment1.service.ClientService;
import edu.utcluj.ro.assignment1.service.ClientUpdateService;
import edu.utcluj.ro.assignment1.service.UserService;

/**
 * Created by Alexandru Trif on 11.04.2017.
 */
public class ServiceUtil {

    private static UserService userService = null;
    private static ClientService clientService = null;
    private static ClientUpdateService clientUpdateService = null;
    private static AccountService accountService = null;

    public static UserService getUserService () {

        if(userService != null) {
            return userService;
        } else {
            return userService = new UserService();
        }
    }

    public static ClientService getClientService () {

        if(clientService != null) {
            return clientService;
        } else {
            return clientService = new ClientService();
        }
    }

    public static ClientUpdateService getClientUpdateService () {

        if(clientUpdateService != null) {
            return clientUpdateService;
        } else {
            return clientUpdateService = new ClientUpdateService();
        }
    }

    public static AccountService getAccountService () {

        if(accountService != null) {
            return accountService;
        } else {
            return accountService = new AccountService();
        }
    }
}
