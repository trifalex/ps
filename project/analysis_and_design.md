# Project Specification
The project is designed for the management of a company.

## Elaboration – Iteration 1.1

### Domain Model
The domain model for my application will contain the following entities
	*Employee*
	*Project/Contract*
	*Company*
	
	Stored in a JSON *company.json*
![company](https://ibb.co/bCpaS5)

### Architectural Design

#### Conceptual Architecture
I used Spring's MVC arhitecture with Thymeleaf for the views and Spring Security for Authorizations and Authentication.
*Model* - encapsulates the application data
*View* - responsible for rendering the model data generating HTML output for the client's browser
*Controller* - processing user requests and building appropriate model then passing it to the view for rendering

My choice was Spring's MVC because it provides a clean division between it's 3 main components making it way easier.

#### Package Design
The Spring Boot provides packages for java where the we can implement model, controllers and services and resources with static package 
that contains css and bootstrap and templates that hold the Thymeleaf html files for the views
![package_diagram](images/package_diagram.JPG)

#### Component and Deployment Diagrams
[Create the component and deployment diagrams.]

## Elaboration – Iteration 1.2

### Design Model

#### Dynamic Behavior
###Admin
The admin can login and acces the administration page
he can then perform crud operations on Employees and Projects
and after that log out and get redirected to the login page
![sequence_diagram](images/sequence_diagram.JPG)


###User
The user logs in on his specific page
and he can view all the company's employees and the 
projects each employee is working on
![communication_diagram](images/communication_diagram.JPG)



#### Class Design
![class_diagram](images/class_diagram.JPG)

#### Data Model
The Company has a list of Contracts and Employees.
Each employee can have a list of Projects (from the list of Company's contracts) assigned.
![data_model](images/data_model.JPG)

#### Unit Testing
TODO
login test
crud tests

## Elaboration – Iteration 2

### Architectural Design Refinement
The Server-side will be a SpringBoot application with Spring Data REST having storage in H2 embedded Spring database
and will be accessed  with Spring JPA and will suply the client-side app by serving json objects to endpoints.
The Client-side will be an Angular 4 with Typescript es6 application that will consume server's endpoints using CORS to obtain data and subscribe to it
with rxJS' Observable to access it assynchronous.
![cs](images/cs.JPG)
![new_package_diagram](images/new_package_diagram.JPG)


### Design Model Refinement
Spring Data REST serves endpoints with requests for basic crud operations by default based on the request method we provide.
We just have to provide entities and their specific Repositories as CrudRepositories.
The H2 Embedded database of Spring is accessed through JPA.
Page requests are matched by CORS.
The list of objects sent from the server is mantained in an async list where the Observable subscribes to in order to read from it.
The Angular4 frontend provides a list of components that are linked together through routes.
Spring Security provides configurations for the Authentication and Authrorities that are managed with JWT.
![s_class_diagram](images/s_class_diagram.JPG)

## Construction and Transition

### System Testing
[Describe how you applied integration testing and present the associated test case scenarios.]

### Future improvements
As future improvements there can be added some interactions between employees, some more frontend styling.

## Bibliography
- [Markdown online editor](http://dillinger.io/)
- [Markdown documentation](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)