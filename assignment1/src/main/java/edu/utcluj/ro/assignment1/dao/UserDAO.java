package edu.utcluj.ro.assignment1.dao;

import edu.utcluj.ro.assignment1.exceptions.DatabaseException;
import edu.utcluj.ro.assignment1.model.User;
import edu.utcluj.ro.assignment1.util.DatabaseUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexandru Trif on 11.04.2017.
 */
public class UserDAO {

    private Connection connection = null;

    public User getUserByEmail (String email) throws DatabaseException {

        connection = DatabaseUtil.getConnection();
        if (connection != null) {

            try {
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM USER WHERE email = ?");

                preparedStatement.setString(1, email);
                ResultSet resultSet = preparedStatement.executeQuery();

                if(resultSet.next()) {
                    User user = returnUserObjectFromDatabase( resultSet);
                    return user;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DatabaseException(e.getMessage());
            }
        } else {
            throw new DatabaseException("Exception caught when trying to open a database connection!");
        }
        return null;
    }

    public User getUserById (int idUser) throws DatabaseException {

        connection = DatabaseUtil.getConnection();
        if(connection != null) {

            try {
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT  * FROM USER WHERE idUser = ?");
                preparedStatement.setInt(1, idUser);
                ResultSet resultSet = preparedStatement.executeQuery();

                if(resultSet.next()) {
                    User user = returnUserObjectFromDatabase(resultSet);
                    return user;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DatabaseException(e.getMessage());
            }
        } else {
            throw new DatabaseException("Exception caught when trying to open a database connection!");
        }
        return null;
    }

    public List<User> getAllEmployees() throws DatabaseException {

        connection = DatabaseUtil.getConnection();
        if(connection != null) {

            try {
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT  * FROM USER WHERE role like '%employee%'");
                ResultSet resultSet = preparedStatement.executeQuery();
                List<User> employees = new ArrayList<>();
                while(resultSet.next()) {

                    User user = returnUserObjectFromDatabase(resultSet);
                    employees.add(user);
                }
                return employees;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DatabaseException(e.getMessage());
            }
        } else {
            throw new DatabaseException("Exception caught when trying to open a database connection!");
        }
    }

    public boolean createUser(User user) throws DatabaseException {

        connection = DatabaseUtil.getConnection();
        if (connection != null) {

            try {
                PreparedStatement preparedStatement =
                        connection.prepareStatement("INSERT INTO " +
                                "USER(firstname, lastName, email, password, address, mobile, role) " +
                                "VALUES (?, ?, ?, ?, ?, ?, ?)");
                preparedStatement.setString(1, user.getFirstName());
                preparedStatement.setString(2, user.getLastName());
                preparedStatement.setString(3, user.getEmail());
                preparedStatement.setString(4, user.getPassword());
                preparedStatement.setString(5, user.getAddress());
                preparedStatement.setString(6, user.getPhoneNumber());
                preparedStatement.setString(7, user.getRole());
                preparedStatement.executeUpdate();

                return true;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DatabaseException(e.getMessage());
            }
        } else {
            throw new DatabaseException("Exception caught when trying to open a database connection!");
        }
    }

    public boolean updateUser (User user) throws DatabaseException {

        connection = DatabaseUtil.getConnection();
        if(connection != null) {

            try {
                PreparedStatement preparedStatement =
                        connection.prepareStatement("UPDATE USER " +
                                "SET firstname = ?, lastName = ?, email = ?, password = ?, address = ?, mobile = ?, role = ?" +
                                "WHERE idUser = ?");
                preparedStatement.setString(1, user.getFirstName());
                preparedStatement.setString(2, user.getLastName());
                preparedStatement.setString(3, user.getEmail());
                preparedStatement.setString(4, user.getPassword());
                preparedStatement.setString(5, user.getAddress());
                preparedStatement.setString(6, user.getPhoneNumber());
                preparedStatement.setString(7, user.getRole());
                preparedStatement.setInt(8, user.getIdUser());
                preparedStatement.executeUpdate();

                return true;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DatabaseException(e.getMessage());
            }
        } else {
            throw new DatabaseException("Exception caught when trying to open a database connection!");
        }
    }

    public boolean deleteUser (int idUser) throws DatabaseException {

        connection = DatabaseUtil.getConnection();
        if(connection != null) {
            try {
                PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM USER WHERE idUser = ?");
                preparedStatement.setInt(1, idUser);
                preparedStatement.executeUpdate();

                return true;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DatabaseException(e.getMessage());
            }
        } else {
            throw new DatabaseException("Exception caught when trying to open a database connection!");
        }
    }

    private User returnUserObjectFromDatabase(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setIdUser(resultSet.getInt("idUser"));
        user.setFirstName(resultSet.getString("firstName"));
        user.setLastName(resultSet.getString("lastName"));
        user.setEmail(resultSet.getString("email"));
        user.setPassword(resultSet.getString("password"));
        user.setAddress(resultSet.getString("address"));
        user.setPhoneNumber(resultSet.getString("mobile"));
        user.setRole(resultSet.getString("role"));
        return user;
    }
}
