# ASSIGNMENT A3



# Objective

The objective of this assignment is to allow students to become familiar with the Client-Server architecture the Observer design pattern and REST API.



# Client-Server

The client-side will be made using Angular4.
The server-side will be a SpringBootApplication with Spring Data REST.


# Observer

The Observer will be be used to subscribe to the list of data we get from server's endpoint.