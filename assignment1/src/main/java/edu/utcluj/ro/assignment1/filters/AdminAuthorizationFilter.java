package edu.utcluj.ro.assignment1.filters;

import edu.utcluj.ro.assignment1.model.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Alexandru Trif on 11.04.2017.
 */

@WebFilter({"/assignment1/guest/*", "/assignment1/employee/*"})
public class AdminAuthorizationFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpSession session = httpServletRequest.getSession();

        if(session != null) {
            User user = (User) session.getAttribute("userLoggedIn");
            if (user != null && user.getRole().equals("admin")) {
                //the logged in admin tries to access a guest or a employee page => the admin will be redirected to admin home page
                httpServletResponse.sendRedirect("/assignment1/admin/home/");
            } else {
                filterChain.doFilter(servletRequest, servletResponse);
            }
        }
    }

    @Override
    public void destroy() {

    }
}
