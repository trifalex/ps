package edu.utcluj.ro.assignment1.model;

import java.sql.Timestamp;

/**
 * Created by Alexandru Trif on 13.04.2017.
 */
public class Client {

    private int idClient;
    private int idUser;
    private String firstName;
    private String lastName;
    private String cardNumber;
    private String address;
    private String personalNumberCode;
    private String lastUpdatedBy;
    private Timestamp creationDate;

    public Client(String firstName, String lastName, String cardNumber, String address, String personalNumberCode, String lastUpdatedBy) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.cardNumber = cardNumber;
        this.address = address;
        this.personalNumberCode = personalNumberCode;
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public Client() {

    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPersonalNumberCode() {
        return personalNumberCode;
    }

    public void setPersonalNumberCode(String personalNumberCode) {
        this.personalNumberCode = personalNumberCode;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }
}
