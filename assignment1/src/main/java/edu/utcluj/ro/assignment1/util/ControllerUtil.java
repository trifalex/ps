package edu.utcluj.ro.assignment1.util;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexandru Trif on 11.04.2017.
 */
public class ControllerUtil {

    public static void handleErrors(Map<String, List<String>> errors, HttpServletRequest request, HttpServletResponse response,
                                    String urlToRedirectResponse, String pathToForwardRequest) throws IOException, ServletException {
        if(errors == null) {
            response.sendRedirect(urlToRedirectResponse);
        } else {

            //add errors to session so they can be shown in the UI
            for(String fieldName : errors.keySet()) {

                String errorsForFieldName = errors.get(fieldName).toString();
                //delete '[' from the beginning of string and ']' from the end of string before storing the errors in session
                request.setAttribute(fieldName, errorsForFieldName.substring(1, errorsForFieldName.length() - 1));
            }
            request.getRequestDispatcher(pathToForwardRequest).forward(request,response);
        }
    }
}
