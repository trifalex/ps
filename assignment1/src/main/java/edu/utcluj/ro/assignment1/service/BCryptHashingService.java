package edu.utcluj.ro.assignment1.service;

import org.springframework.security.crypto.bcrypt.BCrypt;

/**
 * Created by Alexandru Trif on 08.11.2016.
 */
public class BCryptHashingService {

    public static String encryptPassword(String password) {
        String strongSalt = BCrypt.gensalt(12);
        String encryptedPassword = BCrypt.hashpw(password, strongSalt);

        return encryptedPassword;
    }

    public static boolean checkIfPasswordMatchHashcode(String potentialPassword,String hashedPassword){

        return BCrypt.checkpw(potentialPassword, hashedPassword);
    }
}
