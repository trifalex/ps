//package ps.proj.controllers;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import ps.proj.service.EmployeeService;
//
//import java.io.IOException;
//
//@Controller
//@RequestMapping("/admin")
//public class AdminController {
//
//    @Autowired
//    private EmployeeService employeeService;
//
//    @RequestMapping(method = RequestMethod.GET)
//    public String getHome(Model model) throws IOException {
//        model.addAttribute("employees", employeeService.getEmployees());
//        return "admin";
//    }
//}
