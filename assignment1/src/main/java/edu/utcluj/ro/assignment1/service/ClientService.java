package edu.utcluj.ro.assignment1.service;

import edu.utcluj.ro.assignment1.exceptions.DatabaseException;
import edu.utcluj.ro.assignment1.model.Client;
import edu.utcluj.ro.assignment1.util.DaoUtil;
import edu.utcluj.ro.assignment1.validation.ClientValidation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexandru Trif on 13.04.2017.
 */
public class ClientService {

    public Client getClientById(int idClient) throws DatabaseException {

        return DaoUtil.getClientDAO().getClientById(idClient);
    }

    public List<Client> getAllClients() throws DatabaseException {

        return DaoUtil.getClientDAO().getAllClients();
    }

    public Map<String,List<String>> createClient (Client client) {

        Map<String,List<String>> errors;
        errors = ClientValidation.validate(client);

        if(errors.isEmpty()) {

            try {
                DaoUtil.getClientDAO().createClient(client);
            } catch (DatabaseException e) {
                List<String> errorsDescription = new ArrayList<>();
                errorsDescription.add(e.getMessage());
                errors.put("generalError", errorsDescription);
                return errors;
            }
            return null;
        } else {
            return errors;
        }
    }

    public Map<String,List<String>> updateClient (Client client) {

        Map<String,List<String>> errors;
        errors = ClientValidation.validate(client);

        if(errors.isEmpty()) {
            try {
                DaoUtil.getClientDAO().updateClient(client);
            } catch (DatabaseException e) {

                List<String> errorsDescription = new ArrayList<>();
                errorsDescription.add(e.getMessage());
                errors.put("generalError", errorsDescription);
                return errors;
            }
            //there are no errors
            return null;
        } else {
            return errors;
        }
    }


    public boolean deleteClient (int idClient) throws DatabaseException {

        return DaoUtil.getClientDAO().deleteClient(idClient);
    }
}
