package edu.utcluj.ro.assignment1.controller.admin;

import edu.utcluj.ro.assignment1.exceptions.DatabaseException;
import edu.utcluj.ro.assignment1.model.User;
import edu.utcluj.ro.assignment1.util.ServiceUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Alexandru Trif on 11.04.2017.
 */
public class AdminHomeController extends HttpServlet {

    private static final String VIEW_ADMIN_HOME_PATH = "../../../JSP/assignment1/admin/home/home.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            List<User> employees = ServiceUtil.getUserService().getAllEmployees();
            req.setAttribute("employees", employees);
            System.out.println(employees.size());
        } catch (DatabaseException e) {
            e.printStackTrace();
            req.setAttribute("generalError", e.getMessage());
        }

        req.getRequestDispatcher(VIEW_ADMIN_HOME_PATH).forward(req, resp);
    }
}
