package edu.utcluj.ro.assignment1.filters;

import edu.utcluj.ro.assignment1.model.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Alexandru Trif on 11.04.2017.
 */

@WebFilter({"/assignment1/employee/*", "/assignment1/admin/*"})
public class GuestAuthorizationFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpSession session = httpServletRequest.getSession();

        if(session != null) {
            User user = (User) session.getAttribute("userLoggedIn");
            if(user == null) {
                //the guest tries to access a employee/admin page
                httpServletResponse.sendRedirect("/assignment1/guest/");
            } else {
                filterChain.doFilter(servletRequest, servletResponse);
            }
        }
    }

    @Override
    public void destroy() {

    }
}
