package edu.utcluj.ro.assignment1.controller.admin;

import edu.utcluj.ro.assignment1.exceptions.DatabaseException;
import edu.utcluj.ro.assignment1.model.User;
import edu.utcluj.ro.assignment1.util.ServiceUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Alexandru Trif on 13.04.2017.
 */
@WebServlet("/assignment1/admin/employee/delete/*")
public class DeleteEmployeeController extends HttpServlet {

    private static final String VIEW_404_PATH = "../../../../JSP/assignment1/admin/error/404.jsp";
    private static final String VIEW_ADMIN_HOME_PATH = "../../../JSP/assignment1/admin/home/home.jsp";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String[] pathInfo = req.getPathInfo().split("/");
        if(pathInfo.length == 2) {
            String employeeIdToDelete = pathInfo[1];
            try {
                int idUser = Integer.valueOf(employeeIdToDelete);
                User userToDelete = ServiceUtil.getUserService().getUserById(idUser);

                if (userToDelete != null) {
                    ServiceUtil.getUserService().deleteUser(idUser);
                    resp.sendRedirect("/assignment1/admin/home/");
                } else {
                    req.setAttribute("generalError", "There is no user in database with id = " + idUser);
                    req.getRequestDispatcher(VIEW_ADMIN_HOME_PATH).forward(req, resp);
                }

            } catch (NumberFormatException e) {
                req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
            } catch (DatabaseException e) {
                e.printStackTrace();
                req.setAttribute("generalError", e.getMessage());
                req.getRequestDispatcher(VIEW_ADMIN_HOME_PATH).forward(req, resp);
            }

        }  else {
            req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
        }
    }
}
