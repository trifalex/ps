package edu.utcluj.ro.assignment1.service;

import edu.utcluj.ro.assignment1.exceptions.DatabaseException;
import edu.utcluj.ro.assignment1.model.Client;
import edu.utcluj.ro.assignment1.util.DaoUtil;

/**
 * Created by Alexandru Trif on 18.04.2017.
 */
public class ClientUpdateService {

    public boolean createClientUpdate (Client client, String operation) throws DatabaseException {

        return DaoUtil.getClientUpdateDAO().createClientUpdate(client, operation);
    }
}
