package edu.utcluj.ro.assignment1.controller.employee;

import edu.utcluj.ro.assignment1.exceptions.DatabaseException;
import edu.utcluj.ro.assignment1.model.Client;
import edu.utcluj.ro.assignment1.util.ServiceUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Alexandru Trif on 13.04.2017.
 */

public class EmployeeHomeController extends HttpServlet {

    private static final String VIEW_EMPLOYEE_HOME_PATH = "../../../JSP/assignment1/employee/home/home.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            List<Client> clients = ServiceUtil.getClientService().getAllClients();
            req.setAttribute("clients", clients);
        } catch (DatabaseException e) {
            e.printStackTrace();
            req.setAttribute("generalError", e.getMessage());
        }

        req.getRequestDispatcher(VIEW_EMPLOYEE_HOME_PATH).forward(req, resp);
    }
}
