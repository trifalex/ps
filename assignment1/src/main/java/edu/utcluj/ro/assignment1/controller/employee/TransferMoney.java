package edu.utcluj.ro.assignment1.controller.employee;

import edu.utcluj.ro.assignment1.exceptions.DatabaseException;
import edu.utcluj.ro.assignment1.model.Account;
import edu.utcluj.ro.assignment1.model.User;
import edu.utcluj.ro.assignment1.util.ServiceUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Alexandru Trif on 19.04.2017.
 */

@WebServlet("/assignment1/employee/client/transfer-money/")
public class TransferMoney extends HttpServlet {

    private static final String VIEW_EMPLOYEE_TRANSFER_MONEY_PATH = "../../../../JSP/assignment1/employee/transfer_money/transfer_money.jsp";
    private static final String VIEW_404_PATH = "../../../../JSP/assignment1/employee/error/404.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.getRequestDispatcher(VIEW_EMPLOYEE_TRANSFER_MONEY_PATH).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        User employee = (User) req.getSession().getAttribute("userLoggedIn");
        if (employee.getRole().equals("employee")) {

            String accountNumberSender = req.getParameter("accountNumberSender");
            String accountNumberReceiver = req.getParameter("accountNumberReceiver");
            String amount = req.getParameter("amount");

            try {
                Account sender = ServiceUtil.getAccountService().getAccountByAccountNumber(accountNumberSender);
                Account receiver = ServiceUtil.getAccountService().getAccountByAccountNumber(accountNumberReceiver);

                if(sender != null && receiver !=null) {

                    int amountINT = Integer.valueOf(amount);
                    int balance = Integer.valueOf(sender.getBalance()) - amountINT;
                    sender.setBalance("" + balance);
                    balance = Integer.valueOf(receiver.getBalance()) + amountINT;
                    receiver.setBalance("" + balance);

                    ServiceUtil.getAccountService().updateAccount(sender);
                    ServiceUtil.getAccountService().updateAccount(receiver);
                    resp.sendRedirect("/assignment1/employee/home/");
                } else {
                    req.setAttribute("accountNumberSender", accountNumberSender);
                    req.setAttribute("accountNumberReceiver", accountNumberReceiver);
                    req.setAttribute("amount", amount);
                    req.setAttribute("generalError", "Invalid accounts!");
                    req.getRequestDispatcher(VIEW_EMPLOYEE_TRANSFER_MONEY_PATH).forward(req, resp);
                }
            } catch (DatabaseException e) {
                e.printStackTrace();
                req.setAttribute("accountNumberSender", accountNumberSender);
                req.setAttribute("accountNumberReceiver", accountNumberReceiver);
                req.setAttribute("amount", amount);
                req.setAttribute("generalError", e.getMessage());
                req.getRequestDispatcher(VIEW_EMPLOYEE_TRANSFER_MONEY_PATH).forward(req, resp);
            }
        }
    }
}
