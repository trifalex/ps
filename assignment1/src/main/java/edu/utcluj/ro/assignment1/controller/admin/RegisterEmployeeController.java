package edu.utcluj.ro.assignment1.controller.admin;

import edu.utcluj.ro.assignment1.model.User;
import edu.utcluj.ro.assignment1.util.ControllerUtil;
import edu.utcluj.ro.assignment1.util.ServiceUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexandru Trif on 11.04.2017.
 */
public class RegisterEmployeeController extends HttpServlet {

    private static final String VIEW_ADMIN_REGISTER_PATH = "../../../JSP/assignment1/admin/register_employee/register_employee.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.getRequestDispatcher(VIEW_ADMIN_REGISTER_PATH).forward(req, resp);
    }

    @Override
    protected void doPost (HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        User userToRegister = new User(req.getParameter("firstName"), req.getParameter("lastName"), req.getParameter("email"),
                req.getParameter("password"), req.getParameter("confirmPassword"), req.getParameter("address"),
                req.getParameter("phoneNumber"), req.getParameter("role"));
        req.setAttribute("userToRegister", userToRegister);

        Map<String,List<String>> errors = ServiceUtil.getUserService().createUserForRegister(userToRegister);

        ControllerUtil.handleErrors(errors, req, resp, "/assignment1/admin/home/", VIEW_ADMIN_REGISTER_PATH);
    }
}
