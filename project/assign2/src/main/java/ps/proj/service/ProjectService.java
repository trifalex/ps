package ps.proj.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;
import ps.proj.model.Company;
import ps.proj.model.Project;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by atrif on 5/9/2017.
 */
@Component
public class ProjectService {

    public List<Project> getAllContracts() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Company company = mapper.readValue(new FileReader(new File("company.json")), Company.class);

        return company.getContracts();
    }

    public void createContract(Project project) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Company company = mapper.readValue(new FileReader(new File("company.json")), Company.class);
        company.getContracts().add(project);

        FileWriter writer = new FileWriter(new File("company.json"));
        mapper.writeValue(writer, company);

        System.out.println("Project " + project.getIdProject() + " added");
    }

    public void deleteContract(int id) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Company company = mapper.readValue(new FileReader(new File("company.json")), Company.class);
        company.getContracts()
                .removeIf(project -> project.getIdProject() == id);

        FileWriter writer = new FileWriter(new File("company.json"));
        mapper.writeValue(writer, company);
    }
}
