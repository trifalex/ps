import { Component } from '@angular/core'

@Component({
  moduleId: module.id,
  selector: 'home',
  template: `<h1>Catalysts homepage</h1>`
})
export class HomeComponent {
}
