package edu.utcluj.ro.assignment1.validation;

import edu.utcluj.ro.assignment1.model.Client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexandru Trif on 18.04.2017.
 */
public class ClientValidation {

    private static final int MINIM_SIZE_FIRST_AND_LAST_NAME_ALLOWED = 3;
    private static final int MAXIM_SIZE_FIRST_AND_LAST_NAME_ALLOWED = 50;
    private static final int MINIM_SIZE_ADDRESS_ALLOWED = 3;
    private static final int MAXIM_SIZE_ADDRESS_ALLOWED = 50;
    private static final String FIND_ALL_WHITE_SPACES_REGEX_PATTERN = "\\s+";
    private static final String NO_WHITE_SPACES = "";
    private static final String ONE_WHITE_SPACE = " ";
    private static final String FIRST_AND_LAST_NAME_REGEX_PATTERN = "^([A-Za-z])[A-Za-z\\'\\-\\s]*$";

    private static Map<String,List<String>> errors;

    public static Map<String, List<String>> validate(Client client) {

        errors = new HashMap<>();
        validateFirstName(client.getFirstName());
        validateLastName(client.getLastName());
        validateAddress(client.getAddress());

        return errors;
    }

    private static void validateFirstName(String firstName) {

        List<String> errorsDescription = new ArrayList<>();

        // check if the length of first name with no white spaces is at least 3
        if ((firstName.replaceAll(FIND_ALL_WHITE_SPACES_REGEX_PATTERN, NO_WHITE_SPACES))
                .length() < MINIM_SIZE_FIRST_AND_LAST_NAME_ALLOWED) {

            errorsDescription.add("The first name should have at least 3 characters");
            errors.put("firstName", errorsDescription);
        }

        // check if the length -with duplicate white spaces removed- is lesser
        // than 50 (maximum allowed)
        if ((firstName.replaceAll(FIND_ALL_WHITE_SPACES_REGEX_PATTERN, ONE_WHITE_SPACE))
                .length() > MAXIM_SIZE_FIRST_AND_LAST_NAME_ALLOWED) {

            errorsDescription.add("The first name should have maximum 50 characters");
            errors.put("firstName", errorsDescription);
        }

        if (!firstName.matches(FIRST_AND_LAST_NAME_REGEX_PATTERN)) {

            errorsDescription.add("The first name should contain only: a-z, A-Z, ', -");
            errors.put("firstName", errorsDescription);
        }
    }

    private static void validateLastName(String lastName) {

        List<String> errorsDescription = new ArrayList<>();

        // check if the length of last name with no white spaces is at least 3
        if ((lastName.replaceAll(FIND_ALL_WHITE_SPACES_REGEX_PATTERN, NO_WHITE_SPACES))
                .length() < MINIM_SIZE_FIRST_AND_LAST_NAME_ALLOWED) {

            errorsDescription.add("The last name should have at least 3 characters");
            errors.put("lastName", errorsDescription);
        }

        // check if the length -with duplicate white spaces removed- is lesser
        // than 50 (maximum allowed)
        if ((lastName.replaceAll(FIND_ALL_WHITE_SPACES_REGEX_PATTERN, ONE_WHITE_SPACE))
                .length() > MAXIM_SIZE_FIRST_AND_LAST_NAME_ALLOWED) {

            errorsDescription.add("The last name should have maximum 50 characters");
            errors.put("lastName", errorsDescription);
        }

        if (!lastName.matches(FIRST_AND_LAST_NAME_REGEX_PATTERN)) {

            errorsDescription.add("The last name should contain only: a-z, A-Z, ', -");
            errors.put("lastName", errorsDescription);
        }
    }

    private static void validateAddress(String address) {

        List<String> errorsDescription = new ArrayList<>();

        if ((address.replaceAll(FIND_ALL_WHITE_SPACES_REGEX_PATTERN, NO_WHITE_SPACES))
                .length() < MINIM_SIZE_ADDRESS_ALLOWED) {

            errorsDescription.add("The address should have at least 3 characters");
            errors.put("address", errorsDescription);
        }

        if ((address.replaceAll(FIND_ALL_WHITE_SPACES_REGEX_PATTERN, ONE_WHITE_SPACE))
                .length() > MAXIM_SIZE_ADDRESS_ALLOWED) {

            errorsDescription.add("The address should have maximum 50 characters");
            errors.put("address", errorsDescription);
        }
    }
}
