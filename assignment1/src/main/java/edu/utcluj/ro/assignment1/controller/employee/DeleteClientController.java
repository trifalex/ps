package edu.utcluj.ro.assignment1.controller.employee;

import edu.utcluj.ro.assignment1.exceptions.DatabaseException;
import edu.utcluj.ro.assignment1.model.Client;
import edu.utcluj.ro.assignment1.model.User;
import edu.utcluj.ro.assignment1.util.ServiceUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Alexandru Trif on 13.04.2017.
 */
@WebServlet("/assignment1/employee/client/delete/*")
public class DeleteClientController extends HttpServlet {

    private static final String VIEW_404_PATH = "../../../../JSP/assignment1/employee/error/404.jsp";
    private static final String VIEW_EMPLOYEE_HOME_PATH = "../../../JSP/assignment1/employee/home/home.jsp";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        User employee = (User) req.getSession().getAttribute("userLoggedIn");

        if (employee.getRole().equals("employee")) {
            String[] pathInfo = req.getPathInfo().split("/");
            if (pathInfo.length == 2) {
                String clientIdToDelete = pathInfo[1];
                try {
                    int idClient = Integer.valueOf(clientIdToDelete);
                    Client clientToDelete = ServiceUtil.getClientService().getClientById(idClient);

                    if (clientToDelete != null) {
                        clientToDelete.setIdUser(employee.getIdUser());
                        ServiceUtil.getClientUpdateService().createClientUpdate(clientToDelete, "Delete client");
                        ServiceUtil.getClientService().deleteClient(idClient);
                        resp.sendRedirect("/assignment1/employee/home/");
                    } else {
                        req.setAttribute("generalError", "There is no client in database with id = " + idClient);
                        req.getRequestDispatcher(VIEW_EMPLOYEE_HOME_PATH).forward(req, resp);
                    }

                } catch (NumberFormatException e) {
                    req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
                } catch (DatabaseException e) {
                    e.printStackTrace();
                    req.setAttribute("generalError", e.getMessage());
                    req.getRequestDispatcher(VIEW_EMPLOYEE_HOME_PATH).forward(req, resp);
                }

            } else {
                req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
            }
        }
    }
}
