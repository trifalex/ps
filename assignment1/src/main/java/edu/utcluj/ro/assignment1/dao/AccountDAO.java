package edu.utcluj.ro.assignment1.dao;

import edu.utcluj.ro.assignment1.exceptions.DatabaseException;
import edu.utcluj.ro.assignment1.model.Account;
import edu.utcluj.ro.assignment1.model.Client;
import edu.utcluj.ro.assignment1.util.DatabaseUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexandru Trif on 19.04.2017.
 */
public class AccountDAO {

    private Connection connection = null;

    public boolean createAccount (Account account) throws DatabaseException {

        connection = DatabaseUtil.getConnection();
        if (connection != null) {

            try {
                PreparedStatement preparedStatement =
                        connection.prepareStatement("INSERT INTO " +
                                "ACCOUNT(idClient, account_number, account_full_name, account_cvv, balance) " +
                                "VALUES (?, ?, ?, ?, ?)");
                preparedStatement.setInt(1, account.getIdClient());
                preparedStatement.setString(2, account.getAccountNumber());
                preparedStatement.setString(3, account.getAccountFullName());
                preparedStatement.setString(4, account.getAccountCVV());
                preparedStatement.setString(5, account.getBalance());
                preparedStatement.executeUpdate();
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DatabaseException(e.getMessage());
            }
        } else {
            throw new DatabaseException("Exception caught when trying to open a database connection!");
        }
    }

    public List<Account> getAllAccountsByClientId(int idClient) throws DatabaseException {

        connection = DatabaseUtil.getConnection();
        if(connection != null) {

            try {
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT  * FROM ACCOUNT where idClient = ?");
                preparedStatement.setInt(1, idClient);
                ResultSet resultSet = preparedStatement.executeQuery();
                List<Account> accounts = new ArrayList<>();
                while(resultSet.next()) {

                    Account account = returnAccountObjectFromDatabase(resultSet);
                    accounts.add(account);
                }
                return accounts;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DatabaseException(e.getMessage());
            }
        } else {
            throw new DatabaseException("Exception caught when trying to open a database connection!");
        }
    }

    public Account getAccountByAccountNumber (String accountNumber) throws DatabaseException {

        connection = DatabaseUtil.getConnection();
        if(connection != null) {

            try {
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT  * FROM ACCOUNT WHERE account_number = ?");
                preparedStatement.setString(1, accountNumber);
                ResultSet resultSet = preparedStatement.executeQuery();

                if(resultSet.next()) {
                    Account account = returnAccountObjectFromDatabase(resultSet);
                    return account;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DatabaseException(e.getMessage());
            }
        } else {
            throw new DatabaseException("Exception caught when trying to open a database connection!");
        }
        return null;
    }

    public Account getAccountById (int idAccount) throws DatabaseException {

        connection = DatabaseUtil.getConnection();
        if(connection != null) {

            try {
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT  * FROM ACCOUNT WHERE idAccount = ?");
                preparedStatement.setInt(1, idAccount);
                ResultSet resultSet = preparedStatement.executeQuery();

                if(resultSet.next()) {
                    Account account = returnAccountObjectFromDatabase(resultSet);
                    return account;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DatabaseException(e.getMessage());
            }
        } else {
            throw new DatabaseException("Exception caught when trying to open a database connection!");
        }
        return null;
    }

    public boolean updateAccount (Account account) throws DatabaseException {

        connection = DatabaseUtil.getConnection();
        if(connection != null) {

            try {
                PreparedStatement preparedStatement =
                        connection.prepareStatement("UPDATE ACCOUNT " +
                                "SET account_number = ?, account_full_name = ?, account_cvv = ?, balance = ?" +
                                "WHERE idAccount = ?");
                preparedStatement.setString(1, account.getAccountNumber());
                preparedStatement.setString(2, account.getAccountFullName());
                preparedStatement.setString(3, account.getAccountCVV());
                preparedStatement.setString(4, account.getBalance());
                preparedStatement.setInt(5, account.getIdAccount());
                preparedStatement.executeUpdate();

                return true;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DatabaseException(e.getMessage());
            }
        } else {
            throw new DatabaseException("Exception caught when trying to open a database connection!");
        }
    }

    public boolean deleteAccount (int idAccount) throws DatabaseException {

        connection = DatabaseUtil.getConnection();
        if(connection != null) {
            try {
                PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM ACCOUNT WHERE idAccount = ?");
                preparedStatement.setInt(1, idAccount);
                preparedStatement.executeUpdate();

                return true;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DatabaseException(e.getMessage());
            }
        } else {
            throw new DatabaseException("Exception caught when trying to open a database connection!");
        }
    }

    private Account returnAccountObjectFromDatabase (ResultSet resultSet) throws SQLException {
        Account account = new Account();
        account.setIdAccount(resultSet.getInt("idAccount"));
        account.setIdClient(resultSet.getInt("idClient"));
        account.setAccountNumber(resultSet.getString("account_number"));
        account.setAccountFullName(resultSet.getString("account_full_name"));
        account.setAccountCVV(resultSet.getString("account_cvv"));
        account.setBalance(resultSet.getString("balance"));
        return account;
    }
}
