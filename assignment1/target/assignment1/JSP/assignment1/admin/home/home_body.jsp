<%--
  Created by IntelliJ IDEA.
  User: Serban
  Date: 12.04.2017
  Time: 14:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<div class="employees-table">
    <label class="col-sm-8 error-style">${generalError}</label>
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Address</th>
            <th>Phone Number</th>
            <th>Role</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <c:set var="contor" value="1"/>
        <c:forEach items="${employees}" var="employee">
            <tr>
                <th scope="row">${contor}</th>
                <td>${employee.idUser}</td>
                <td>${employee.firstName}</td>
                <td>${employee.lastName}</td>
                <td>${employee.email}</td>
                <td>${employee.address}</td>
                <td>${employee.phoneNumber}</td>
                <td>${employee.role}</td>
                <td>
                    <form method="get" id="updateEmployeeForm${contor}"
                          action="${pageContext.request.contextPath}/assignment1/admin/employee/update/${employee.idUser}"></form>
                    <a class="no-hover" href="#"
                       onclick="document.getElementById('updateEmployeeForm${contor}').submit();">Update</a>
                </td>
                <td>
                    <a class="no-hover" href="#" data-toggle="modal" data-target="#deleteModal${contor}">Delete</a>
                    <!-- modal for deleting employee-->
                    <div class="modal fade" tabindex="-1"
                         id="deleteModal${contor}" role="dialog"
                         aria-labelledby="deleteModal${contor}">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title"
                                        id="modalLabel${contor}">
                                        <span class="black-color">Delete ${employee.firstName}</span>
                                    </h4>
                                </div>
                                <div class="modal-body">
															<span class="primary-color">Are you sure you want
																to delete ${employee.firstName} ${employee.lastName}?</span>
                                </div>
                                <div class="modal-footer">
                                    <form method="post" class="form-horizontal" id="deleteEmployeeForm${contor}"
                                          action="${pageContext.request.contextPath}/assignment1/admin/employee/delete/${employee.idUser}">
                                        <button type="button" class="btn btn-danger"
                                                data-dismiss="modal">Close
                                        </button>
                                        <button type="submit" class="btn btn-primary">Delete
                                            Employee
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <c:set var="contor" value="${contor + 1}"/>
        </c:forEach>
        </tbody>
    </table>
</div>

</body>
</html>
