<%--
  Created by IntelliJ IDEA.
  User: Serban
  Date: 13.04.2017
  Time: 12:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="http://getbootstrap.com/examples/sticky-footer-navbar/sticky-footer-navbar.css">
    <link rel="stylesheet" type="text/css" href="<c:url value="${pageContext.request.contextPath}/style/bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="${pageContext.request.contextPath}/style/bootstrap/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="${pageContext.request.contextPath}/style/CSS/Employee/employee_style.css" />">

    <script src="<c:url value="${pageContext.request.contextPath}/style/JS/jquery-3.1.1.min.js" />"></script>
    <script src="<c:url value="${pageContext.request.contextPath}/style/bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js" />"></script>
    <title>Home</title>
</head>
<body>

<div class="employee-wrapper">
    <div class="employee-wrapper-page">
        <jsp:include page="${pageContext.request.contextPath}/JSP/common/employee/employee_header.jsp"/>
        <div class="employee-body">
            <label class="col-sm-8 error-style">${generalError}</label>
            <jsp:include page="${pageContext.request.contextPath}/JSP/assignment1/employee/home/home_body.jsp" />
        </div>
    </div>
    <jsp:include page="${pageContext.request.contextPath}/JSP/common/employee/employee_footer.jsp"/>
</div>

</body>
</html>
