import {Component, OnInit} from "@angular/core";

import {Employee} from "./employee";
import {EmployeeService} from "./employee.service";
import {ActivatedRoute, Params} from "@angular/router";
import {Location} from "@angular/common";
import "rxjs/add/operator/switchMap";
@Component({
  selector: 'employee-detail',
  templateUrl: 'employee-detail.component.html',
})
export class EmployeeDetailComponent implements OnInit {
  employee: Employee;

  constructor(private employeeService: EmployeeService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  ngOnInit(): void {
    this.route.params
      .switchMap((params: Params) => this.employeeService.getEmployee(+params['id']))
      .subscribe(employee => this.employee = employee);
  }

  save(): void {
    this.employeeService.update(this.employee)
      .then(() => this.goBack());
  }

  goBack(): void {
    this.location.back();
  }
}
