<%--
  Created by IntelliJ IDEA.
  User: Serban
  Date: 03.11.2016
  Time: 18:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="http://getbootstrap.com/examples/sticky-footer-navbar/sticky-footer-navbar.css">
    <link rel="stylesheet" type="text/css" href="<c:url value="${pageContext.request.contextPath}/style/bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="${pageContext.request.contextPath}/style/bootstrap/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="${pageContext.request.contextPath}/style/CSS/Guest/guest_style.css" />">

    <script src="<c:url value="${pageContext.request.contextPath}/style/JS/jquery-3.1.1.min.js" />"></script>
    <script src="<c:url value="${pageContext.request.contextPath}/style/bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js" />"></script>
    <title>Home</title>
</head>
<body>

<div class="guest-wrapper">
    <div class="guest-wrapper-page">
        <jsp:include page="${pageContext.request.contextPath}/JSP/common/guest/guest_header.jsp"/>
        <div class="guest-body">

            <pre>
                Application Description
                Use JAVA/C# API to design and implement an application for the front desk employees of a
                bank. The application should have two types of users (a regular user represented by the front
                desk employee and an administrator user) which have to provide a username and a password in
                order to use the application.
                The regular user can perform the following operations:
                - Add/update/view client information (name, identity card number, personal numerical
                code, address, etc.).
                - Create/update/delete/view client account (account information: identification number,
                type, amount of money, date of creation).
                - Transfer money between accounts.
                - Process utilities bills.
                The administrator user can perform the following operations:
                - CRUD on employees’ information.
                - Generate reports for a particular period containing the activities performed by an
                employee.
            </pre>
            <pre>
                Application Constraints
                • The data will be stored in a database. Use the Layers architectural pattern to organize
                your application. Use a domain logic pattern (transaction script or domain model) / a data
                source hybrid pattern (table module, active record) and a data source pure pattern (table
                data gateway, row data gateway, data mapper) most suitable for the application
                • All the inputs of the application will be validated against invalid data before submitting
                the data and saving it in the database.
            </pre>
            <pre>
                Requirements
                - Create the analysis and design document (see the template).
                - Implement and test the application.
            </pre>
            <pre>
                Deliverables
                - Analysis and design document.
                - Implementation source files.
                - SQL script for creating and populating the database with initial values.
                - Readme file that describes the installation process of the application and how to use it:
                o how to install your application on a clean computer
                o how to access your application and with what users
                o images with all use cases and their scenarios implemented
            </pre>
        </div>
    </div>
    <jsp:include page="${pageContext.request.contextPath}/JSP/common/guest/guest_footer.jsp"/>
</div>

</body>
</html>

