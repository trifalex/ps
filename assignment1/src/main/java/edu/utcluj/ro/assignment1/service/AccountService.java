package edu.utcluj.ro.assignment1.service;

import edu.utcluj.ro.assignment1.exceptions.DatabaseException;
import edu.utcluj.ro.assignment1.model.Account;
import edu.utcluj.ro.assignment1.util.DaoUtil;
import edu.utcluj.ro.assignment1.validation.AccountValidator;
import edu.utcluj.ro.assignment1.validation.ClientValidation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexandru Trif on 19.04.2017.
 */
public class AccountService {

    public List<Account> getAllAccountsByClientId(int idClient) throws DatabaseException {

        return DaoUtil.getAccountDAO().getAllAccountsByClientId(idClient);
    }

    public Map<String,List<String>> createAccount (Account account) throws DatabaseException {

        Map<String,List<String>> errors;
        errors = AccountValidator.validate(account);

        if(errors.isEmpty()) {

            try {
                DaoUtil.getAccountDAO().createAccount(account);
            } catch (DatabaseException e) {
                List<String> errorsDescription = new ArrayList<>();
                errorsDescription.add(e.getMessage());
                errors.put("generalError", errorsDescription);
                return errors;
            }
            return null;
        } else {
            return errors;
        }
    }

    public Map<String,List<String>> updateAccount (Account account) throws DatabaseException {

        Map<String,List<String>> errors;
        errors = AccountValidator.validate(account);

        if(errors.isEmpty()) {

            try {
                DaoUtil.getAccountDAO().updateAccount(account);
            } catch (DatabaseException e) {
                List<String> errorsDescription = new ArrayList<>();
                errorsDescription.add(e.getMessage());
                errors.put("generalError", errorsDescription);
                return errors;
            }
            return null;
        } else {
            return errors;
        }
    }

    public boolean deleteAccount (int idAccount) throws DatabaseException {

        return DaoUtil.getAccountDAO().deleteAccount(idAccount);
    }

    public Account getAccountById (int idAccount) throws DatabaseException {

        return DaoUtil.getAccountDAO().getAccountById(idAccount);
    }

    public Account getAccountByAccountNumber (String accountNumber) throws DatabaseException {

        return DaoUtil.getAccountDAO().getAccountByAccountNumber(accountNumber);
    }
}
