package edu.utcluj.ro.assignment1.dao;

import edu.utcluj.ro.assignment1.exceptions.DatabaseException;
import edu.utcluj.ro.assignment1.model.Client;
import edu.utcluj.ro.assignment1.util.DatabaseUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Alexandru Trif on 18.04.2017.
 */
public class ClientUpdateDAO {

    private Connection connection = null;

    public boolean createClientUpdate (Client client, String operation) throws DatabaseException {

        connection = DatabaseUtil.getConnection();
        if (connection != null) {

            try {
                PreparedStatement preparedStatement =
                        connection.prepareStatement("INSERT INTO " +
                                "CLIENT_UPDATE(idClient, idUser, operation) " +
                                "VALUES (?, ?, ?)");
                preparedStatement.setInt(1, client.getIdClient());
                preparedStatement.setInt(2, client.getIdUser());
                preparedStatement.setString(3, operation);
                preparedStatement.executeUpdate();
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DatabaseException(e.getMessage());
            }
        } else {
            throw new DatabaseException("Exception caught when trying to open a database connection!");
        }
    }
}
