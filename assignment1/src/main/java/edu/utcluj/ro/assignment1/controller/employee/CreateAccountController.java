package edu.utcluj.ro.assignment1.controller.employee;

import edu.utcluj.ro.assignment1.exceptions.DatabaseException;
import edu.utcluj.ro.assignment1.model.Account;
import edu.utcluj.ro.assignment1.model.Client;
import edu.utcluj.ro.assignment1.model.User;
import edu.utcluj.ro.assignment1.util.ControllerUtil;
import edu.utcluj.ro.assignment1.util.ServiceUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexandru Trif on 13.04.2017.
 */

@WebServlet("/assignment1/employee/client/create-account/*")
public class CreateAccountController extends HttpServlet {

    private static final String VIEW_CREATE_CLIENT_ACCOUNT_PATH = "../../../../JSP/assignment1/employee/create_account/create_account.jsp";
    private static final String VIEW_404_PATH = "../../../../JSP/assignment1/employee/error/404.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String[] pathInfo = req.getPathInfo().split("/");
        if(pathInfo.length == 2) {

            String clientIdToUpdate = pathInfo[1];

            try {

                int idClient = Integer.valueOf(clientIdToUpdate);
                Client client = ServiceUtil.getClientService().getClientById(idClient);

                if (client != null) {

                    req.setAttribute("client", client);
                    req.getRequestDispatcher(VIEW_CREATE_CLIENT_ACCOUNT_PATH).forward(req, resp);
                } else {
                    req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
                }
            } catch(NumberFormatException e) {
                req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
            } catch (DatabaseException e) {
                e.printStackTrace();
                req.setAttribute("generalError", e.getMessage());
                req.getRequestDispatcher(VIEW_CREATE_CLIENT_ACCOUNT_PATH).forward(req, resp);
            }
        } else {
            req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
        }
    }

    @Override
    protected void doPost (HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        User employee = (User) req.getSession().getAttribute("userLoggedIn");
        String[] pathInfo = req.getPathInfo().split("/");
        if (employee.getRole().equals("employee")) {
            if (pathInfo.length == 2) {
                String clientId = pathInfo[1];
                try {
                    int idClient = Integer.valueOf(clientId);

                    Client client = ServiceUtil.getClientService().getClientById(idClient);

                    if (client != null) {
                        Account account = new Account(idClient, req.getParameter("accountNumber"), req.getParameter("accountFullName"),
                                req.getParameter("accountCVV"), req.getParameter("balance"));
                        Map<String, List<String>> errors = ServiceUtil.getAccountService().createAccount(account);
                        if (errors == null) {
                            ServiceUtil.getClientUpdateService().createClientUpdate(client, "Create account for client");
                        }
                        ControllerUtil.handleErrors(errors, req, resp, "/assignment1/employee/home/", VIEW_CREATE_CLIENT_ACCOUNT_PATH);
                    }
                } catch (NumberFormatException e) {
                    Client client = new Client();
                    client.setIdClient(Integer.valueOf(clientId));
                    req.setAttribute("client", client);
                    req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
                } catch (DatabaseException e) {
                    e.printStackTrace();
                    Client client = new Client();
                    client.setIdClient(Integer.valueOf(clientId));
                    req.setAttribute("client", client);
                    req.setAttribute("generalError", e.getMessage());
                    req.getRequestDispatcher(VIEW_CREATE_CLIENT_ACCOUNT_PATH).forward(req, resp);
                }
            } else {
                req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
            }

        }
    }
}
