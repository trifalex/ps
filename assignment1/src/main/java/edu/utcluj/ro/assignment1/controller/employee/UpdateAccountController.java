package edu.utcluj.ro.assignment1.controller.employee;

import edu.utcluj.ro.assignment1.exceptions.DatabaseException;
import edu.utcluj.ro.assignment1.model.Account;
import edu.utcluj.ro.assignment1.model.Client;
import edu.utcluj.ro.assignment1.model.User;
import edu.utcluj.ro.assignment1.util.ControllerUtil;
import edu.utcluj.ro.assignment1.util.ServiceUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexandru Trif on 13.04.2017.
 */
@WebServlet("/assignment1/employee/client/update-account/*")
public class UpdateAccountController extends HttpServlet {

    private static final String VIEW_UPDATE_CLIENT_ACCOUNT_PATH = "../../../../JSP/assignment1/employee/update_account/update_account.jsp";
    private static final String VIEW_404_PATH = "../../../../JSP/assignment1/employee/error/404.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String[] pathInfo = req.getPathInfo().split("/");
        if(pathInfo.length == 2) {

            String accountId = pathInfo[1];

            try {

                int idAccount = Integer.valueOf(accountId);
                Account account = ServiceUtil.getAccountService().getAccountById(idAccount);

                if (account != null) {

                    req.setAttribute("account", account);
                    req.getRequestDispatcher(VIEW_UPDATE_CLIENT_ACCOUNT_PATH).forward(req, resp);
                } else {
                    req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
                }
            } catch(NumberFormatException e) {
                req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
            } catch (DatabaseException e) {
                e.printStackTrace();
                req.setAttribute("generalError", e.getMessage());
                req.getRequestDispatcher(VIEW_UPDATE_CLIENT_ACCOUNT_PATH).forward(req, resp);
            }
        } else {
            req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
        }
    }

    @Override
    protected void doPost (HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        User employee = (User) req.getSession().getAttribute("userLoggedIn");
        String[] pathInfo = req.getPathInfo().split("/");
        if (employee.getRole().equals("employee")) {
            if (pathInfo.length == 2) {
                String accountId = pathInfo[1];
                try {
                    int idAccount = Integer.valueOf(accountId);

                    Account account = ServiceUtil.getAccountService().getAccountById(idAccount);

                    if (account != null) {

                        account.setAccountNumber(req.getParameter("accountNumber"));
                        account.setBalance(req.getParameter("balance"));
                        account.setAccountFullName(req.getParameter("accountFullName"));
                        account.setAccountCVV(req.getParameter("accountCVV"));

                        Map<String, List<String>> errors = ServiceUtil.getAccountService().updateAccount(account);
                        if (errors == null) {
                            Client client = new Client();
                            client.setIdClient(account.getIdClient());
                            client.setIdUser(employee.getIdUser());
                            ServiceUtil.getClientUpdateService().createClientUpdate(client, "Update account" + accountId + " for client");
                            ServiceUtil.getClientUpdateService().createClientUpdate(client, "Create account for client");
                        }
                        ControllerUtil.handleErrors(errors, req, resp, "/assignment1/employee/client/view/" + account.getIdClient(), VIEW_UPDATE_CLIENT_ACCOUNT_PATH);
                    }
                } catch (NumberFormatException e) {
                    req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
                } catch (DatabaseException e) {
                    e.printStackTrace();
                    req.setAttribute("generalError", e.getMessage());
                    req.getRequestDispatcher(VIEW_UPDATE_CLIENT_ACCOUNT_PATH).forward(req, resp);
                }
            } else {
                req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
            }

        }
    }
}
