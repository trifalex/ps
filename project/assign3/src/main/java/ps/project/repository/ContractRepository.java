package ps.project.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import ps.project.entity.Contract;

@RepositoryRestResource(collectionResourceRel = "contracts", path = "contracts")
public interface ContractRepository extends CrudRepository<Contract, Long> {

    @Query("from Contract c where lower(c.name) like CONCAT('%', lower(:name), '%')")
    public Iterable<Contract> findByName(@Param("name") String name);

//    @Query("from Contract c inner join Projects p on c.id=p.contract where p.employee like CONCAT('%', :id, '%')")
//    public Iterable<Contract> findProjectsForEmployee(@Param("id") int id);
}
