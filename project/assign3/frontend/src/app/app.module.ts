import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";

import {AppComponent} from "./app.component";
import {EmployeeDetailComponent} from "./employee-detail.component";
import {EmployeeService} from "./employee.service";
import {EmployeesComponent} from "./employees.component";
import {DashboardComponent} from "./dashboard.component";
import {AppRoutingModule} from "./app-routing.module";
import {EmployeeSearchComponent} from "./employee-search.component";
import {HomeComponent} from "./home.component";
import {LoginComponent} from "./login.component";
import {AuthenticationService} from "./authentication.service";
import {CanActivateAuthGuard} from "./can-activate.authguard";

@NgModule({
  declarations: [
    AppComponent,
    EmployeeDetailComponent,
    EmployeesComponent,
    EmployeeSearchComponent,
    DashboardComponent,
    HomeComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
  ],
  providers: [
    EmployeeService,
    AuthenticationService,
    CanActivateAuthGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
