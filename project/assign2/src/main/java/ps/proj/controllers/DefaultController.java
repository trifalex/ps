package ps.proj.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ps.proj.model.Employee;
import ps.proj.model.Project;
import ps.proj.service.EmployeeService;
import ps.proj.service.ProjectService;

import java.io.IOException;

@Controller
public class DefaultController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private ProjectService projectService;

    @GetMapping("/")
    public String home1() {
        return "/index";
    }

    @GetMapping("/index")
    public String home() {
        return "/index";
    }

    @GetMapping("/admin")
    public String admin(Model model) throws IOException {
        model.addAttribute("employee", new Employee());
        model.addAttribute("project", new Project());
        model.addAttribute("employees", employeeService.getEmployees());
        model.addAttribute("contracts", projectService.getAllContracts());
        return "admin";
    }

    @RequestMapping(value = "/admin/employee/add", method = RequestMethod.POST)
    public String insertEmployee(@ModelAttribute("employee") Employee employee) throws IOException {
        this.employeeService.createEmployee(employee);
        return "redirect:/admin";
    }

    @RequestMapping(value = "/admin/employee/doDelete", method = RequestMethod.POST)
    public String removeEmployee(@RequestParam int idEmployee) throws IOException {
        this.employeeService.deleteEmployee(idEmployee);
        return "redirect:/admin";
    }

    @RequestMapping(value = "edit/{id}")
    public String editEmployee(@PathVariable("id") int id, Model model) throws IOException {
        model.addAttribute("employee", employeeService.getEmployeeById(id));
        model.addAttribute("employees", employeeService.getEmployees());
        return "admin";
    }

    @PostMapping(value = "/admin/contract/add")
    public String insertContract(@ModelAttribute("contract") Project project) throws IOException {
        this.projectService.createContract(project);
        return "redirect:/admin";
    }

    @PostMapping(value = "/admin/contract/doDelete")
    public String removeContract(@RequestParam int idProject) throws IOException {
        this.projectService.deleteContract(idProject);
        return "redirect:/admin";
    }

    @GetMapping("/user")
    public String user(Model model) throws IOException {
        model.addAttribute("employees", employeeService.getEmployees());
        model.addAttribute("contracts", projectService.getAllContracts());
        return "/user";
    }


    @GetMapping("/login")
    public String login() {
        return "/login";
    }

    @GetMapping("/403")
    public String error403() {
        return "/error/403";
    }

}
