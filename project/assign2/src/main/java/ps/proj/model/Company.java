package ps.proj.model;

import java.util.Date;
import java.util.List;

/**
 * Created by atrif on 5/7/2017.
 */
public class Company {

    private List<Employee> employees;
    private String name;
    private Date registryDate;
    private List<Project> contracts;

    public List<Project> getContracts() {
        return contracts;
    }

    public void setContracts(List<Project> contracts) {
        this.contracts = contracts;
    }


    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(Date registryDate) {
        this.registryDate = registryDate;
    }
}
