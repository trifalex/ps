package edu.utcluj.ro.assignment1.controller.employee;

import edu.utcluj.ro.assignment1.exceptions.DatabaseException;
import edu.utcluj.ro.assignment1.model.Client;
import edu.utcluj.ro.assignment1.model.User;
import edu.utcluj.ro.assignment1.util.DaoUtil;
import edu.utcluj.ro.assignment1.util.ServiceUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexandru Trif on 13.04.2017.
 */
@WebServlet("/assignment1/employee/client/update/*")
public class UpdateClientController extends HttpServlet {

    private static final String VIEW_EMPLOYEE_UPDATE_CLIENT_PATH = "../../../../JSP/assignment1/employee/update_client/update_client.jsp";
    private static final String VIEW_404_PATH = "../../../../JSP/assignment1/employee/error/404.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String[] pathInfo = req.getPathInfo().split("/");
        if(pathInfo.length == 2) {

            String clientIdToUpdate = pathInfo[1];

            try {

                int idClient = Integer.valueOf(clientIdToUpdate);
                Client client = ServiceUtil.getClientService().getClientById(idClient);

                if (client != null) {
                    req.setAttribute("client", client);
                    req.getRequestDispatcher(VIEW_EMPLOYEE_UPDATE_CLIENT_PATH).forward(req, resp);
                } else {
                    req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
                }
            } catch(NumberFormatException e) {
                req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
            } catch (DatabaseException e) {
                e.printStackTrace();
                req.setAttribute("generalError", e.getMessage());
                req.getRequestDispatcher(VIEW_EMPLOYEE_UPDATE_CLIENT_PATH).forward(req, resp);
            }
        } else {
            req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String[] pathInfo = req.getPathInfo().split("/");
        User employee = (User) req.getSession().getAttribute("userLoggedIn");

        if (employee.getRole().equals("employee")) {
            Client clientInSession = new Client(req.getParameter("firstName"), req.getParameter("lastName"), req.getParameter("cardNumber"),
                    req.getParameter("address"), req.getParameter("personalNumberCode"),
                    employee.getFirstName() + " " + employee.getLastName());
            if (pathInfo.length == 2) {
                String clientIdToUpdate = pathInfo[1];

                try {
                    int idClient = Integer.valueOf(clientIdToUpdate);
                    Client clientToUpdate = ServiceUtil.getClientService().getClientById(idClient);

                    if (clientToUpdate != null) {

                        clientInSession.setIdClient(idClient);

                        Map<String, List<String>> errors = ServiceUtil.getClientService().updateClient(clientInSession);

                        if (errors == null) {
                            try {
                                Client client2 = DaoUtil.getClientDAO().getClientById(idClient);
                                client2.setIdUser(employee.getIdUser());
                                ServiceUtil.getClientUpdateService().createClientUpdate(client2, "Update client");
                            } catch (DatabaseException e) {
                                errors  = new HashMap<>();
                                List<String> errorsDescription = new ArrayList<>();
                                errorsDescription.add(e.getMessage());
                                errors.put("generalError", errorsDescription);
                            }
                            resp.sendRedirect("/assignment1/employee/home/");
                        } else {

                            //add errors to session so they can be shown in the UI
                            for (String fieldName : errors.keySet()) {

                                String errorsForFieldName = errors.get(fieldName).toString();
                                //delete '[' from the beginning of string and ']' from the end of string before storing the errors in session
                                req.setAttribute(fieldName, errorsForFieldName.substring(1, errorsForFieldName.length() - 1));
                            }
                            req.setAttribute("client", clientInSession);
                            req.getRequestDispatcher(VIEW_EMPLOYEE_UPDATE_CLIENT_PATH).forward(req, resp);
                        }
                    } else {
                        req.setAttribute("generalError", "There is no client in database with id = " + idClient);
                        req.setAttribute("client", clientInSession);
                        req.getRequestDispatcher(VIEW_EMPLOYEE_UPDATE_CLIENT_PATH).forward(req, resp);
                    }
                } catch (NumberFormatException e) {
                    req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
                } catch (DatabaseException e) {
                    e.printStackTrace();
                    req.setAttribute("generalError", e.getMessage());
                    req.setAttribute("client", clientInSession);
                    req.getRequestDispatcher(VIEW_EMPLOYEE_UPDATE_CLIENT_PATH).forward(req, resp);
                }
            } else {
                req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
            }
        }
    }
}
