<%--
  Created by IntelliJ IDEA.
  User: Alexandru
  Date: 19.04.2017
  Time: 1:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link rel="stylesheet" type="text/css"
          href="http://getbootstrap.com/examples/sticky-footer-navbar/sticky-footer-navbar.css">
    <link rel="stylesheet" type="text/css"
          href="<c:url value="${pageContext.request.contextPath}/style/bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css" />">
    <link rel="stylesheet" type="text/css"
          href="<c:url value="${pageContext.request.contextPath}/style/bootstrap/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="${pageContext.request.contextPath}/style/CSS/Guest/guest_style.css" />">

    <script src="<c:url value="${pageContext.request.contextPath}/style/JS/jquery-3.1.1.min.js" />"></script>
    <script src="<c:url value="${pageContext.request.contextPath}/style/bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js" />"></script>
    <title>Update Account</title>
</head>
<body>

<div class="guest-wrapper">
    <div class="guest-wrapper-page">
        <jsp:include page="${pageContext.request.contextPath}/JSP/common/employee/employee_header.jsp"/>
        <div class="guest-body">
            <div class="register-login-form">
                <label class="col-sm-8 error-style">${generalError}</label>
                <form class="form-horizontal" method="post"
                      action="${pageContext.request.contextPath}/assignment1/employee/client/update-account/${account.idAccount}">
                    <div class="form-group"></div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Account Number</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="Account Number"
                                   name="accountNumber" value="${account.accountNumber}">
                        </div>
                        <label class="col-sm-8 error-style">${accountNumber}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Account Full Name</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="Account Full Name"
                                   name="accountFullName" value="${account.accountFullName}">
                        </div>
                        <label class="col-sm-8 error-style">${accountFullName}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Account CVV</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="Account CVV" name="accountCVV"
                                   value="${account.accountCVV}">
                        </div>
                        <label class="col-sm-8 error-style">${accountCVV}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Balance</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="Balance" name="balance"
                                   value="${account.balance}">
                        </div>
                        <label class="col-sm-8 error-style">${balance}</label>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn">Update Account</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <jsp:include page="${pageContext.request.contextPath}/JSP/common/employee/employee_footer.jsp"/>
</div>
</body>
</html>
