<%--
  Created by IntelliJ IDEA.
  User: Alexandru
  Date: 13.04.2017
  Time: 12:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<div class="clients-table">
    <label class="col-sm-8 error-style">${generalError}</label>
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Client ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Card Number</th>
            <th>Address</th>
            <th>Personal Number Code</th>
            <th>Last Updated By</th>
            <th>Creation Date</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <c:set var="contor" value="1"/>
        <c:forEach items="${clients}" var="client">
            <tr>
                <th scope="row">${contor}</th>
                <td>
                    <form method="get" id="viewClientForm${contor}"
                          action="${pageContext.request.contextPath}/assignment1/employee/client/view/${client.idClient}"></form>
                    <a class="no-hover" href="#"
                       onclick="document.getElementById('viewClientForm${contor}').submit();">View Client ID ${client.idClient}</a>
                </td>
                <td>${client.firstName}</td>
                <td>${client.lastName}</td>
                <td>${client.cardNumber}</td>
                <td>${client.address}</td>
                <td>${client.personalNumberCode}</td>
                <td>${client.lastUpdatedBy}</td>
                <td>${client.creationDate}</td>
                <td>
                    <form method="get" id="updateClientForm${contor}"
                          action="${pageContext.request.contextPath}/assignment1/employee/client/update/${client.idClient}"></form>
                    <a class="no-hover" href="#"
                       onclick="document.getElementById('updateClientForm${contor}').submit();">Update</a>
                </td>
                <td>
                    <a class="no-hover" href="#" data-toggle="modal" data-target="#deleteModal${contor}">Delete</a>
                    <!-- modal for deleting client-->
                    <div class="modal fade" tabindex="-1"
                         id="deleteModal${contor}" role="dialog"
                         aria-labelledby="deleteModal${contor}">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title"
                                        id="modalLabel${contor}">
                                        <span class="black-color">Delete ${client.firstName}</span>
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    <span class="primary-color">Are you sure you want
                                        to delete ${client.firstName} ${client.lastName}?</span>
                                </div>
                                <div class="modal-footer">
                                    <form method="post" class="form-horizontal" id="deleteClientForm${contor}"
                                          action="${pageContext.request.contextPath}/assignment1/employee/client/delete/${client.idClient}">
                                        <button type="button" class="btn btn-danger"
                                                data-dismiss="modal">Close
                                        </button>
                                        <button type="submit" class="btn btn-primary">Delete Client</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
                <td>
                    <form method="get" id="createAccountForm${contor}"
                          action="${pageContext.request.contextPath}/assignment1/employee/client/create-account/${client.idClient}"></form>
                    <a class="no-hover" href="#"
                       onclick="document.getElementById('createAccountForm${contor}').submit();">Create Account</a>
                </td>
            </tr>
            <c:set var="contor" value="${contor + 1}"/>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>
