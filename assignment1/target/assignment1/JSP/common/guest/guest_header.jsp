<%--
  Created by IntelliJ IDEA.
  User: Serban
  Date: 03.11.2016
  Time: 18:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<html>
<head>

</head>
<body>
    <div class="guest-header">
        <div class="guest-header-bar">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand link-text-color" href="<c:url value="${pageContext.request.contextPath}/assignment1/guest/" />">Assignment 1</a>
                </div>
                <div class="collapse navbar-collapse"
                     id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav ">
                        <li class="no-hover"><a class="no-hover link-text-color" href="<c:url value="${pageContext.request.contextPath}/assignment1/guest/" />">Home</a></li>
                        <li class="no-hover"><a class="no-hover link-text-color" href="<c:url value="${pageContext.request.contextPath}/assignment1/guest/login/" />">Log In</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
