package edu.utcluj.ro.assignment1.service;

import edu.utcluj.ro.assignment1.exceptions.DatabaseException;
import edu.utcluj.ro.assignment1.exceptions.UniqueEmailException;
import edu.utcluj.ro.assignment1.model.User;
import edu.utcluj.ro.assignment1.util.DaoUtil;
import edu.utcluj.ro.assignment1.validation.EmployeeValidation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexandru Trif on 11.04.2017.
 */
public class UserService {

    public User getUserByEmail (String email) throws DatabaseException {

        return DaoUtil.getUserDAO().getUserByEmail(email);
    }

    public User getUserById(int idUser) throws DatabaseException {

        return DaoUtil.getUserDAO().getUserById(idUser);
    }

    public List<User> getAllEmployees() throws DatabaseException {

        return DaoUtil.getUserDAO().getAllEmployees();
    }

    public Map<String,List<String>> createUserForRegister(User user) {

        Map<String,List<String>> errors;
        errors = EmployeeValidation.validate(user);

        if(errors.isEmpty()) {

            //there are no errors
            user.setPassword(BCryptHashingService.encryptPassword(user.getPassword()));

            try {
                DaoUtil.getUserDAO().createUser(user);
            } catch (DatabaseException e) {

                if(e instanceof UniqueEmailException) {

                    List<String> errorsDescription = new ArrayList<>();
                    errorsDescription.add("This email is already in use!");
                    errors.put("email", errorsDescription);
                    return errors;
                } else {

                    List<String> errorsDescription = new ArrayList<>();
                    errorsDescription.add(e.getMessage());
                    errors.put("generalError", errorsDescription);
                    return errors;
                }
            }

            //there are no errors
            return null;
        } else {
            return errors;
        }
    }

    public Map<String,List<String>> updateEmployee (User user) {

        Map<String,List<String>> errors;
        errors = EmployeeValidation.validate(user);

        if(errors.isEmpty()) {

            //there are no errors
            user.setPassword(BCryptHashingService.encryptPassword(user.getPassword()));

            try {
                DaoUtil.getUserDAO().updateUser(user);
            } catch (DatabaseException e) {

                if(e instanceof UniqueEmailException) {

                    List<String> errorsDescription = new ArrayList<>();
                    errorsDescription.add("This email is already in use!");
                    errors.put("email", errorsDescription);
                    return errors;
                } else {

                    List<String> errorsDescription = new ArrayList<>();
                    errorsDescription.add(e.getMessage());
                    errors.put("generalError", errorsDescription);
                    return errors;
                }
            }

            //there are no errors
            return null;
        } else {
            return errors;
        }
    }

    public boolean deleteUser (int idUser) throws DatabaseException {

        return DaoUtil.getUserDAO().deleteUser(idUser);
    }
}
