package edu.utcluj.ro.assignment1.exceptions;

/**
 * Created by Alexandru Trif on 11.04.2017.
 */
public class DatabaseException extends Exception {

    public DatabaseException () {}

    public DatabaseException (String message) {
        super(message);
    }

}
