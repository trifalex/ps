package edu.utcluj.ro.assignment1.controller.guest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Alexandru Trif on 11.04.2017.
 */
public class HomeController extends HttpServlet {

    private static final String VIEW_GUEST_HOME_PATH = "../../JSP/assignment1/home.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(VIEW_GUEST_HOME_PATH).forward(req, resp);
    }
}
