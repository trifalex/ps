package edu.utcluj.ro.assignment1.model;

/**
 * Created by Alexandru Trif on 13.04.2017.
 */
public class Account {

    private int idAccount;
    private int idClient;
    private String accountNumber;
    private String accountFullName;
    private String accountCVV;
    private String balance;

    public int getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(int idAccount) {
        this.idAccount = idAccount;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountFullName() {
        return accountFullName;
    }

    public void setAccountFullName(String accountFullName) {
        this.accountFullName = accountFullName;
    }

    public String getAccountCVV() {
        return accountCVV;
    }

    public void setAccountCVV(String accountCVV) {
        this.accountCVV = accountCVV;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public Account() {

    }

    public Account(int idClient, String accountNumber, String accountFullName, String accountCVV, String balance) {
        this.idClient = idClient;
        this.accountNumber = accountNumber;
        this.accountFullName = accountFullName;
        this.accountCVV = accountCVV;
        this.balance = balance;
    }
}
