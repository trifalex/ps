package edu.utcluj.ro.assignment1.dao;

import edu.utcluj.ro.assignment1.exceptions.DatabaseException;
import edu.utcluj.ro.assignment1.model.Client;
import edu.utcluj.ro.assignment1.util.DatabaseUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexandru Trif on 13.04.2017.
 */
public class ClientDAO {

    private Connection connection = null;

    public Client getClientById (int idClient) throws DatabaseException {

        connection = DatabaseUtil.getConnection();
        if(connection != null) {

            try {
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT  * FROM CLIENT WHERE idClient = ?");
                preparedStatement.setInt(1, idClient);
                ResultSet resultSet = preparedStatement.executeQuery();

                if(resultSet.next()) {
                    Client client = returnClientsObjectFromDatabase(resultSet);
                    return client;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DatabaseException(e.getMessage());
            }
        } else {
            throw new DatabaseException("Exception caught when trying to open a database connection!");
        }
        return null;
    }

    public Client getClientByPersonalNumberCode (String personalNumberCode) throws DatabaseException {

        connection = DatabaseUtil.getConnection();
        if(connection != null) {

            try {
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT  * FROM CLIENT WHERE personal_nr_code = ?");
                preparedStatement.setString(1, personalNumberCode);
                ResultSet resultSet = preparedStatement.executeQuery();

                if(resultSet.next()) {
                    Client client = returnClientsObjectFromDatabase(resultSet);
                    return client;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DatabaseException(e.getMessage());
            }
        } else {
            throw new DatabaseException("Exception caught when trying to open a database connection!");
        }
        return null;
    }

    public boolean createClient(Client client) throws DatabaseException {

        connection = DatabaseUtil.getConnection();
        if (connection != null) {

            try {
                PreparedStatement preparedStatement =
                        connection.prepareStatement("INSERT INTO " +
                                "CLIENT(firstname, lastName, card_number, address, personal_nr_code, last_updated_by, creation_date) " +
                                "VALUES (?, ?, ?, ?, ?, ?, ?)");
                preparedStatement.setString(1, client.getFirstName());
                preparedStatement.setString(2, client.getLastName());
                preparedStatement.setString(3, client.getCardNumber());
                preparedStatement.setString(4, client.getAddress());
                preparedStatement.setString(5, client.getPersonalNumberCode());
                preparedStatement.setString(6, client.getLastUpdatedBy());
                preparedStatement.setTimestamp(7, client.getCreationDate());
                preparedStatement.executeUpdate();
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DatabaseException(e.getMessage());
            }
        } else {
            throw new DatabaseException("Exception caught when trying to open a database connection!");
        }
    }

    public boolean updateClient (Client client) throws DatabaseException {

        connection = DatabaseUtil.getConnection();
        if(connection != null) {

            try {
                PreparedStatement preparedStatement =
                        connection.prepareStatement("UPDATE CLIENT " +
                                "SET firstname = ?, lastName = ?, card_number = ?, address = ?, personal_nr_code = ?, last_updated_by = ?" +
                                "WHERE idClient = ?");
                preparedStatement.setString(1, client.getFirstName());
                preparedStatement.setString(2, client.getLastName());
                preparedStatement.setString(3, client.getCardNumber());
                preparedStatement.setString(4, client.getAddress());
                preparedStatement.setString(5, client.getPersonalNumberCode());
                preparedStatement.setString(6, client.getLastUpdatedBy());
                preparedStatement.setInt(7, client.getIdClient());
                preparedStatement.executeUpdate();

                return true;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DatabaseException(e.getMessage());
            }
        } else {
            throw new DatabaseException("Exception caught when trying to open a database connection!");
        }
    }

    public boolean deleteClient (int idClient) throws DatabaseException {

        connection = DatabaseUtil.getConnection();
        if(connection != null) {
            try {
                PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM ACCOUNT WHERE idClient = ?");
                preparedStatement.setInt(1, idClient);
                preparedStatement.executeUpdate();
                preparedStatement = connection.prepareStatement("DELETE FROM CLIENT WHERE idClient = ?");
                preparedStatement.setInt(1, idClient);
                preparedStatement.executeUpdate();

                return true;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DatabaseException(e.getMessage());
            }
        } else {
            throw new DatabaseException("Exception caught when trying to open a database connection!");
        }
    }

    public List<Client> getAllClients() throws DatabaseException {

        connection = DatabaseUtil.getConnection();
        if(connection != null) {

            try {
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT  * FROM CLIENT");
                ResultSet resultSet = preparedStatement.executeQuery();
                List<Client> clients = new ArrayList<>();
                while(resultSet.next()) {

                    Client client = returnClientsObjectFromDatabase(resultSet);
                    clients.add(client);
                }
                return clients;
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DatabaseException(e.getMessage());
            }
        } else {
            throw new DatabaseException("Exception caught when trying to open a database connection!");
        }
    }

    private Client returnClientsObjectFromDatabase (ResultSet resultSet) throws SQLException {
        Client client = new Client();
        client.setIdClient(resultSet.getInt("idClient"));
        client.setFirstName(resultSet.getString("firstname"));
        client.setLastName(resultSet.getString("lastname"));
        client.setCardNumber(resultSet.getString("card_number"));
        client.setAddress(resultSet.getString("address"));
        client.setPersonalNumberCode(resultSet.getString("personal_nr_code"));
        client.setLastUpdatedBy(resultSet.getString("last_updated_by"));
        client.setCreationDate(resultSet.getTimestamp("creation_date"));
        return client;
    }
}
