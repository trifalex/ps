package edu.utcluj.ro.assignment1.controller.employee;

import edu.utcluj.ro.assignment1.exceptions.DatabaseException;
import edu.utcluj.ro.assignment1.model.Client;
import edu.utcluj.ro.assignment1.model.User;
import edu.utcluj.ro.assignment1.util.ControllerUtil;
import edu.utcluj.ro.assignment1.util.DaoUtil;
import edu.utcluj.ro.assignment1.util.ServiceUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by Alexandru Trif on 13.04.2017.
 */
public class RegisterClientController extends HttpServlet {

    private static final String VIEW_EMPLOYEE_REGISTER_PATH = "../../../JSP/assignment1/employee/register_client/register_client.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.getRequestDispatcher(VIEW_EMPLOYEE_REGISTER_PATH).forward(req, resp);
    }

    @Override
    protected void doPost (HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        User employee = (User) req.getSession().getAttribute("userLoggedIn");

        if (employee.getRole().equals("employee")) {
            Client client = new Client(req.getParameter("firstName"), req.getParameter("lastName"), req.getParameter("cardNumber"),
                    req.getParameter("address"), req.getParameter("personalNumberCode"),
                    employee.getFirstName() + " " + employee.getLastName());

            client.setCreationDate(new Timestamp(new Date().getTime()));
            req.setAttribute("clientToCreate", client);
            Map<String, List<String>> errors = ServiceUtil.getClientService().createClient(client);
            if (errors == null) {
                try {
                    Client client2 = DaoUtil.getClientDAO().getClientByPersonalNumberCode(client.getPersonalNumberCode());
                    client2.setIdUser(employee.getIdUser());
                    ServiceUtil.getClientUpdateService().createClientUpdate(client2, "Create client");
                } catch (DatabaseException e) {
                    errors  = new HashMap<>();
                    List<String> errorsDescription = new ArrayList<>();
                    errorsDescription.add(e.getMessage());
                    errors.put("generalError", errorsDescription);
                }
            }
            ControllerUtil.handleErrors(errors, req, resp, "/assignment1/employee/home/", VIEW_EMPLOYEE_REGISTER_PATH);
        } else {
            req.setAttribute("generalError", "Error!");
            req.getRequestDispatcher(VIEW_EMPLOYEE_REGISTER_PATH).forward(req, resp);
        }
    }
}
