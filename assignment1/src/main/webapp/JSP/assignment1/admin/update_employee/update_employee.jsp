<%--
  Created by IntelliJ IDEA.
  User: Alexandru
  Date: 13.04.2017
  Time: 9:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="http://getbootstrap.com/examples/sticky-footer-navbar/sticky-footer-navbar.css">
    <link rel="stylesheet" type="text/css" href="<c:url value="${pageContext.request.contextPath}/style/bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="${pageContext.request.contextPath}/style/bootstrap/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="${pageContext.request.contextPath}/style/CSS/Guest/guest_style.css" />">

    <script src="<c:url value="${pageContext.request.contextPath}/style/JS/jquery-3.1.1.min.js" />"></script>
    <script src="<c:url value="${pageContext.request.contextPath}/style/bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js" />"></script>
    <title>Update ${user.firstName}</title>
</head>
<body>
<div class="guest-wrapper">
    <div class="guest-wrapper-page">
        <jsp:include page="${pageContext.request.contextPath}/JSP/common/admin/admin_header.jsp"/>
        <div class="guest-body">
            <div class="register-login-form">
                <form class="form-horizontal" method="post" action="${pageContext.request.contextPath}/assignment1/admin/employee/update/${user.idUser}">
                    <div class="form-group">
                        <label class="col-sm-1 control-label">First name</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="First name" name="firstName" value="${user.firstName}">
                        </div>
                        <label class="col-sm-8 error-style">${firstName}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Last name</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="Last name" name="lastName" value="${user.lastName}">
                        </div>
                        <label class="col-sm-8 error-style">${lastName}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Email</label>
                        <div class="col-sm-3">
                            <input type="email" class="form-control" placeholder="Email" name="email" value="${user.email}">
                        </div>
                        <label class="col-sm-8 error-style">${email}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Password</label>
                        <div class="col-sm-3">
                            <input type="password" class="form-control" placeholder="Password" name="password">
                        </div>
                        <label class="col-sm-8 error-style">${password}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Confirm password</label>
                        <div class="col-sm-3">
                            <input type="password" class="form-control" placeholder="Confirm password" name="confirmPassword">
                        </div>
                        <label class="col-sm-8 error-style">${confirmPassword}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Address</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="Address" name="address" value="${user.address}">
                        </div>
                        <label class="col-sm-8 error-style">${address}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Phone number</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="Phone number" name="phoneNumber" value="${user.phoneNumber}">
                        </div>
                        <label class="col-sm-8 error-style">${phoneNumber}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Role</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="Role" name="role"
                                   value="${user.role}">
                        </div>
                        <label class="col-sm-8 error-style">${role}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-8 error-style">${generalError}</label>
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn">Update Employee</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <jsp:include page="${pageContext.request.contextPath}/JSP/common/admin/admin_footer.jsp"/>
</div>
</body>
</html>
