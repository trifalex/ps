import {Component, OnInit} from "@angular/core";
import {EmployeeService} from "./employee.service";
import {Router} from "@angular/router";
import {Employee} from "./employee";

@Component({
  selector: 'my-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css'],
  providers: [EmployeeService],
})
export class EmployeesComponent implements OnInit {
  selectedEmployee: Employee;
  employees: Employee[];

  constructor(private employeeService: EmployeeService,
              private router: Router) {
  }

  getEmployees(): void {
    this.employeeService.getEmployees()
      .then(
        employees => this.employees = employees,
        error => {
          this.router.navigate(['login']);
          console.error('An error occurred in heroes component, navigating to login: ', error);
        });
  }

  add(name: string): void {
    name = name.trim();
    if (!name) {
      return;
    }
    this.employeeService.create(name)
      .then(employee => {
        this.employees.push(employee);
        this.selectedEmployee = null;
      });
  }

  delete(employee: Employee): void {
    this.employeeService
      .delete(employee.id)
      .then(() => {
        this.employees = this.employees.filter(h => h !== employee);
        if (this.selectedEmployee === employee) {
          this.selectedEmployee = null;
        }
      });
  }

  ngOnInit(): void {
    this.getEmployees();
  }

  onSelect(employee: Employee): void {
    this.selectedEmployee = employee;
  }

  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedEmployee.id]);
  }
}
