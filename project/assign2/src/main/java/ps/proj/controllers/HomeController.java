//package ps.proj.controllers;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import ps.proj.service.EmployeeService;
//
//import java.io.IOException;
//
///**
// * Created by atrif on 5/7/2017.
// */
//@Controller
//@RequestMapping("/")
//public class HomeController {
//
//    @Autowired
//    private EmployeeService employeeService;
//
//    @RequestMapping(method = RequestMethod.GET)
//    public String getHome(Model model) throws IOException {
//        model.addAttribute("employees", employeeService.getEmployees());
//        return "index";
//    }
//
//    @RequestMapping(value = "/post", method = RequestMethod.GET)
//    public String getName(@RequestParam("name") String name) {
//        System.out.println(name);
//        return "index";
//    }
//}
