import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";

import {DashboardComponent} from "./dashboard.component";
import {EmployeeDetailComponent} from "./employee-detail.component";
import {EmployeesComponent} from "./employees.component";
import {HomeComponent} from "./home.component";
import {LoginComponent} from "./login.component";
import {CanActivateAuthGuard} from "./can-activate.authguard";

const routes: Routes = [
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'dashboard', component: DashboardComponent, canActivate: [CanActivateAuthGuard]},
  {path: 'detail/:id', component: EmployeeDetailComponent, canActivate: [CanActivateAuthGuard]},
  {path: 'employees', component: EmployeesComponent, canActivate: [CanActivateAuthGuard]},
  {path: 'login', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
