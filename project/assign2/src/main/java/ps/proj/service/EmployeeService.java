package ps.proj.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;
import ps.proj.model.Company;
import ps.proj.model.Employee;
import ps.proj.model.Project;

import java.io.*;
import java.util.List;

/**
 * Created by atrif on 5/9/2017.
 */
@Component
public class EmployeeService {


    public Employee getEmployeeById(int idEmployee) throws FileNotFoundException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        Company company1 = mapper.readValue(new FileReader(new File("company.json")), Company.class);

        Employee searchedEmployee = company1.getEmployees().
                stream().
                filter(employee -> employee.getIdEmployee() == idEmployee).findAny().orElse(null);

        return searchedEmployee;
    }

    public List<Employee> getEmployees() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Company company = mapper.readValue(new FileReader(new File("company.json")), Company.class);

        return company.getEmployees();
    }

    public void createEmployee(Employee employee) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Company company = mapper.readValue(new FileReader(new File("company.json")), Company.class);
        company.getEmployees().add(employee);

        FileWriter writer = new FileWriter(new File("company.json"));
        mapper.writeValue(writer, company);

        System.out.println("Employee added");
    }

    public void deleteEmployee(int id) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Company company = mapper.readValue(new FileReader(new File("company.json")), Company.class);
        company.getEmployees()
                .removeIf(employee -> employee.getIdEmployee() == id);

        FileWriter writer = new FileWriter(new File("company.json"));
        mapper.writeValue(writer, company);
    }

    public void addProjectToEmployee(Employee employee, Project project) throws IOException{
        ObjectMapper mapper = new ObjectMapper();
        Company company = mapper.readValue(new FileReader(new File("company.json")), Company.class);

        Employee searchedEmployee = company.getEmployees().
                stream().
                filter(e -> e.getIdEmployee() == employee.getIdEmployee()).findAny().orElse(null);

        searchedEmployee.getProjects().add(project);

        FileWriter writer = new FileWriter(new File("company.json"));
        mapper.writeValue(writer, company);

        System.out.println("Project " + project.getIdProject() + " added to employee " +employee.getIdEmployee());
    }
}
