package edu.utcluj.ro.assignment1.validation;

import edu.utcluj.ro.assignment1.model.Account;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexandru Trif on 19.04.2017.
 */

public class AccountValidator {

    private static final String CARD_CODE_REGEX = "^([0-9]{16})$";
    private static final String VCC_REGEX = "^([0-9]{3})$";
    private static final String FULL_NAME_REGEX = "([A-Za-z]{6,60})";

    private static Map<String,List<String>> errors;

    public static Map<String, List<String>> validate(Account account) {

        errors = new HashMap<>();
        validateAccountNumber(account.getAccountNumber());
        validateAccountCVV(account.getAccountCVV());
        validateAccountFullName(account.getAccountFullName());

        return errors;
    }

    public static void validateAccountNumber(String accountNumber) {

        List<String> errorsDescription = new ArrayList<>();

        if (!accountNumber.matches(CARD_CODE_REGEX)) {

            errorsDescription.add("The account number should have 16 digits!");
            errors.put("accountNumber", errorsDescription);
        }
    }

    public static void validateAccountCVV(String accountCVV) {

        List<String> errorsDescription = new ArrayList<>();

        if (!accountCVV.matches(VCC_REGEX)) {
            errorsDescription.add("The vcc should have 3 digits!");
            errors.put("accountCVV", errorsDescription);
        }
    }

    public static void validateAccountFullName(String accountFullName) {

        List<String> errorsDescription = new ArrayList<>();

        if (!accountFullName.matches(FULL_NAME_REGEX)) {
            errorsDescription.add("The full name should contain only: a-z, A-Z and should have between 6 - 60 characters");
            errors.put("accountFullName", errorsDescription);
        }
    }
}
