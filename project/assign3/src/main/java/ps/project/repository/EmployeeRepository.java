package ps.project.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import ps.project.entity.Employee;

@RepositoryRestResource(collectionResourceRel = "employees", path = "employees")
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    @Query("from Employee e where lower(e.name) like CONCAT('%', lower(:name), '%')")
    public Iterable<Employee> findByName(@Param("name") String name);

}
