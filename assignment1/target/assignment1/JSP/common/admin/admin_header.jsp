<%--
  Created by IntelliJ IDEA.
  User: Serban
  Date: 03.11.2016
  Time: 18:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <div class="admin-header">
        <div class="admin-header-bar">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand link-text-color" href="<c:url value="${pageContext.request.contextPath}/assignment1/admin/home/" />">Assignment 1</a>
                </div>
                <div class="collapse navbar-collapse"
                     id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav ">
                        <li class="no-hover"><a class="no-hover link-text-color" href="<c:url value="${pageContext.request.contextPath}/assignment1/admin/home/" />">Home</a></li>
                        <li class="no-hover"><a class="no-hover link-text-color" href="<c:url value="${pageContext.request.contextPath}/assignment1/admin/register-employee/" />">Register New Employee</a></li>
                        <li class="no-hover">
                            <a class="no-hover link-text-color" href="#" onclick="document.getElementById('logoutForm').submit();">Log Out</a>
                        </li>
                        <form method="post" id="logoutForm" action="${pageContext.request.contextPath}/assignment1/admin/logout/"></form>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
