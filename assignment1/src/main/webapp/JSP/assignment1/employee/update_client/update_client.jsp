<%--
  Created by IntelliJ IDEA.
  User: Alexandru
  Date: 07.11.2016
  Time: 19:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="http://getbootstrap.com/examples/sticky-footer-navbar/sticky-footer-navbar.css">
    <link rel="stylesheet" type="text/css" href="<c:url value="${pageContext.request.contextPath}/style/bootstrap/bootstrap-3.3.7-dist/css/bootstrap.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="${pageContext.request.contextPath}/style/bootstrap/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="${pageContext.request.contextPath}/style/CSS/Guest/guest_style.css" />">

    <script src="<c:url value="${pageContext.request.contextPath}/style/JS/jquery-3.1.1.min.js" />"></script>
    <script src="<c:url value="${pageContext.request.contextPath}/style/bootstrap/bootstrap-3.3.7-dist/js/bootstrap.min.js" />"></script>
    <title>Register</title>
</head>
<body>

<div class="guest-wrapper">
    <div class="guest-wrapper-page">
        <jsp:include page="${pageContext.request.contextPath}/JSP/common/employee/employee_header.jsp"/>
        <div class="guest-body">
            <div class="register-login-form">
                <form class="form-horizontal" method="post" action="${pageContext.request.contextPath}/assignment1/employee/client/update/${client.idClient}">
                    <div class="form-group">
                        <label class="col-sm-1 control-label">First name</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="First name" name="firstName" value="${client.firstName}">
                        </div>
                        <label class="col-sm-8 error-style">${firstName}</label>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-1 control-label">Last name</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" placeholder="Last name" name="lastName" value="${client.lastName}">
                    </div>
                    <label class="col-sm-8 error-style">${lastName}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Card Number</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="Card Number" name="cardNumber" value="${client.cardNumber}">
                        </div>
                        <label class="col-sm-8 error-style">${cardNumber}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Address</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="Address" name="address" value="${client.address}">
                        </div>
                        <label class="col-sm-8 error-style">${address}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Personal Number Code</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="Personal Number Code" name="personalNumberCode" value="${client.personalNumberCode}">
                        </div>
                        <label class="col-sm-8 error-style">${personalNumberCode}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-8 error-style">${generalError}</label>
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn">Register Client</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <jsp:include page="${pageContext.request.contextPath}/JSP/common/employee/employee_footer.jsp"/>
</div>
</body>
</html>
