package edu.utcluj.ro.assignment1.validation;

import edu.utcluj.ro.assignment1.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexandru Trif on 11.04.2017.
 */
public class EmployeeValidation {

    private static final int MINIM_SIZE_FIRST_AND_LAST_NAME_ALLOWED = 3;
    private static final int MAXIM_SIZE_FIRST_AND_LAST_NAME_ALLOWED = 50;
    private static final int MINIM_SIZE_EMAIL_ALLOWED = 10;
    private static final int MAXIM_SIZE_EMAIL_ALLOWED = 50;
    private static final int MINIM_LENGTH_PASSWORD_ALLOWED = 10;
    private static final int MAXIM_LENGTH_PASSWORD_ALLOWED = 50;
    private static final int MINIM_SIZE_ADDRESS_ALLOWED = 3;
    private static final int MAXIM_SIZE_ADDRESS_ALLOWED = 50;
    private static final int EXACT_SIZE_MOBILE_PHONE_NUMBER = 12;
    private static final String FIND_ALL_WHITE_SPACES_REGEX_PATTERN = "\\s+";
    private static final String NO_WHITE_SPACES = "";
    private static final String ONE_WHITE_SPACE = " ";
    private static final String FIRST_AND_LAST_NAME_REGEX_PATTERN = "^([A-Za-z])[A-Za-z\\'\\-\\s]*$";
    private static final String EMAIL_REGEX_PATTERN = "^([_A-Za-z0-9\\.]{8,20})(@)([_A-Za-z0-9-]{3,20})(\\.)([A-Za-z0-9-]{2,})$";
    private static final String PASSWORD_REGEX_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=]).{10,50}$";
    private static final String MOBILE_PHONE_NUMBER_REGEX_PATTERN = "^(\\+)(4)(0)(7)([0-9]{8})$";

    private static Map<String,List<String>> errors;

    public static Map<String, List<String>> validate(User user) {

        errors = new HashMap<>();
        validateFirstName(user.getFirstName());
        validateLastName(user.getLastName());
        validateEmail(user.getEmail());
        validatePassword(user.getPassword());
        validateConfirmedPassword(user.getPassword(), user.getConfirmedPassword());
        validateAddress(user.getAddress());
        validatePhoneNumber(user.getPhoneNumber());

        return errors;
    }

    private static void validateFirstName(String firstName) {

        List<String> errorsDescription = new ArrayList<>();

        // check if the length of first name with no white spaces is at least 3
        if ((firstName.replaceAll(FIND_ALL_WHITE_SPACES_REGEX_PATTERN, NO_WHITE_SPACES))
                .length() < MINIM_SIZE_FIRST_AND_LAST_NAME_ALLOWED) {

            errorsDescription.add("The first name should have at least 3 characters");
            errors.put("firstName", errorsDescription);
        }

        // check if the length -with duplicate white spaces removed- is lesser
        // than 50 (maximum allowed)
        if ((firstName.replaceAll(FIND_ALL_WHITE_SPACES_REGEX_PATTERN, ONE_WHITE_SPACE))
                .length() > MAXIM_SIZE_FIRST_AND_LAST_NAME_ALLOWED) {

            errorsDescription.add("The first name should have maximum 50 characters");
            errors.put("firstName", errorsDescription);
        }

        if (!firstName.matches(FIRST_AND_LAST_NAME_REGEX_PATTERN)) {

            errorsDescription.add("The first name should contain only: a-z, A-Z, ', -");
            errors.put("firstName", errorsDescription);
        }
    }

    private static void validateLastName(String lastName) {

        List<String> errorsDescription = new ArrayList<>();

        // check if the length of last name with no white spaces is at least 3
        if ((lastName.replaceAll(FIND_ALL_WHITE_SPACES_REGEX_PATTERN, NO_WHITE_SPACES))
                .length() < MINIM_SIZE_FIRST_AND_LAST_NAME_ALLOWED) {

            errorsDescription.add("The last name should have at least 3 characters");
            errors.put("lastName", errorsDescription);
        }

        // check if the length -with duplicate white spaces removed- is lesser
        // than 50 (maximum allowed)
        if ((lastName.replaceAll(FIND_ALL_WHITE_SPACES_REGEX_PATTERN, ONE_WHITE_SPACE))
                .length() > MAXIM_SIZE_FIRST_AND_LAST_NAME_ALLOWED) {

            errorsDescription.add("The last name should have maximum 50 characters");
            errors.put("lastName", errorsDescription);
        }

        if (!lastName.matches(FIRST_AND_LAST_NAME_REGEX_PATTERN)) {

            errorsDescription.add("The last name should contain only: a-z, A-Z, ', -");
            errors.put("lastName", errorsDescription);
        }
    }

    private static void validateEmail(String email) {

        List<String> errorsDescription = new ArrayList<>();

        if ((email.replaceAll(FIND_ALL_WHITE_SPACES_REGEX_PATTERN, NO_WHITE_SPACES))
                .length() < MINIM_SIZE_EMAIL_ALLOWED) {

            errorsDescription.add("The email should have at least 10 characters");
            errors.put("email", errorsDescription);
        }

        if ((email.replaceAll(FIND_ALL_WHITE_SPACES_REGEX_PATTERN, ONE_WHITE_SPACE))
                .length() > MAXIM_SIZE_EMAIL_ALLOWED) {

            errorsDescription.add("The email should have maximum 50 characters");
            errors.put("email", errorsDescription);
        }

        if (!email.matches(EMAIL_REGEX_PATTERN)) {

            errorsDescription.add("The email should contain only: a-z, A-Z, 0-9, '.', '-', '_', '@'");
            errors.put("email", errorsDescription);
        }
    }

    private static void validatePassword(String password) {

        List<String> errorsDescription = new ArrayList<>();

        if ((password.replaceAll(FIND_ALL_WHITE_SPACES_REGEX_PATTERN, NO_WHITE_SPACES))
                .length() < MINIM_LENGTH_PASSWORD_ALLOWED) {

            errorsDescription.add("The password should have at least 10 characters");
            errors.put("password", errorsDescription);
        }

        if ((password.replaceAll(FIND_ALL_WHITE_SPACES_REGEX_PATTERN, NO_WHITE_SPACES))
                .length() > MAXIM_LENGTH_PASSWORD_ALLOWED) {

            errorsDescription.add("The password should have maximum 50 characters");
            errors.put("password", errorsDescription);
        }

        if (password.contains(FIND_ALL_WHITE_SPACES_REGEX_PATTERN)) {

            errorsDescription.add("The password can not contain white spaces");
            errors.put("password", errorsDescription);
        }

        if (!password.matches(PASSWORD_REGEX_PATTERN)) {

            errorsDescription.add("The password should contain at least: 1 digit, a lower case letter, a upper case letter, a special character from !@#$%^&+");
            errors.put("password", errorsDescription);
        }
    }

    private static void validateConfirmedPassword(String password, String confirmedPassword) {

        List<String> errorsDescription = new ArrayList<>();

        if(!password.equals(confirmedPassword)) {
            errorsDescription.add("The passwords do not match");
            errors.put("confirmPassword", errorsDescription);
        }
    }

    private static void validateAddress(String address) {

        List<String> errorsDescription = new ArrayList<>();

        if ((address.replaceAll(FIND_ALL_WHITE_SPACES_REGEX_PATTERN, NO_WHITE_SPACES))
                .length() < MINIM_SIZE_ADDRESS_ALLOWED) {

            errorsDescription.add("The address should have at least 3 characters");
            errors.put("address", errorsDescription);
        }

        if ((address.replaceAll(FIND_ALL_WHITE_SPACES_REGEX_PATTERN, ONE_WHITE_SPACE))
                .length() > MAXIM_SIZE_ADDRESS_ALLOWED) {

            errorsDescription.add("The address should have maximum 50 characters");
            errors.put("address", errorsDescription);
        }
    }

    private static void validatePhoneNumber(String phoneNumber) {

        List<String> errorsDescription = new ArrayList<>();

        if ((phoneNumber.replaceAll(FIND_ALL_WHITE_SPACES_REGEX_PATTERN, NO_WHITE_SPACES))
                .length() != EXACT_SIZE_MOBILE_PHONE_NUMBER) {

            errorsDescription.add("The phone number should contain exactly 12 digits");
            errors.put("phoneNumber", errorsDescription);
        }

        // for a better reading of a number e.g: +40799 999 999
        if (!(phoneNumber.replaceAll(FIND_ALL_WHITE_SPACES_REGEX_PATTERN, NO_WHITE_SPACES))
                .matches(MOBILE_PHONE_NUMBER_REGEX_PATTERN)) {

            errorsDescription.add("The phone number pattern is: +407...");
            errors.put("phoneNumber", errorsDescription);
        }
    }
}
