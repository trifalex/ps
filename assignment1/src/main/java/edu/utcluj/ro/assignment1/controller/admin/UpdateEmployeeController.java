package edu.utcluj.ro.assignment1.controller.admin;

import edu.utcluj.ro.assignment1.exceptions.DatabaseException;
import edu.utcluj.ro.assignment1.model.User;
import edu.utcluj.ro.assignment1.util.ServiceUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexandru Trif on 12.04.2017.
 */
@WebServlet("/assignment1/admin/employee/update/*")
public class UpdateEmployeeController extends HttpServlet {

    private static final String VIEW_ADMIN_UPDATE_EMPLOYEE_PATH = "../../../../JSP/assignment1/admin/update_employee/update_employee.jsp";
    private static final String VIEW_404_PATH = "../../../../JSP/assignment1/admin/error/404.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String[] pathInfo = req.getPathInfo().split("/");
        if(pathInfo.length == 2) {

            String employeeIdToUpdate = pathInfo[1];

            try {

                int idUser = Integer.valueOf(employeeIdToUpdate);
                User user = ServiceUtil.getUserService().getUserById(idUser);

                if (user != null) {
                    req.setAttribute("user", user);
                    req.getRequestDispatcher(VIEW_ADMIN_UPDATE_EMPLOYEE_PATH).forward(req, resp);
                } else {
                    req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
                }
            } catch(NumberFormatException e) {
                req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
            } catch (DatabaseException e) {
                e.printStackTrace();
                req.setAttribute("generalError", e.getMessage());
                req.getRequestDispatcher(VIEW_ADMIN_UPDATE_EMPLOYEE_PATH).forward(req, resp);
            }
        } else {
            req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String[] pathInfo = req.getPathInfo().split("/");
        User userInSession = new User(req.getParameter("firstName"), req.getParameter("lastName"), req.getParameter("email"),
                req.getParameter("password"), req.getParameter("confirmPassword"), req.getParameter("address"),
                req.getParameter("phoneNumber"), req.getParameter("role"));
        if(pathInfo.length == 2) {
            String employeeIdToUpdate = pathInfo[1];

            try {
                int idUser = Integer.valueOf(employeeIdToUpdate);
                User userToUpdate = ServiceUtil.getUserService().getUserById(idUser);

                if (userToUpdate != null) {

                    userInSession.setIdUser(userToUpdate.getIdUser());
                    userToUpdate.setFirstName(req.getParameter("firstName"));
                    userToUpdate.setLastName(req.getParameter("lastName"));
                    userToUpdate.setEmail(req.getParameter("email"));
                    userToUpdate.setPassword(req.getParameter("password"));
                    userToUpdate.setConfirmedPassword(req.getParameter("confirmPassword"));
                    userToUpdate.setAddress(req.getParameter("address"));
                    userToUpdate.setPhoneNumber(req.getParameter("phoneNumber"));
                    userToUpdate.setRole(req.getParameter("role"));

                    Map<String,List<String>> errors = ServiceUtil.getUserService().updateEmployee(userToUpdate);

                    if(errors == null) {
                        resp.sendRedirect("/assignment1/admin/home/");
                    } else {

                        //add errors to session so they can be shown in the UI
                        for(String fieldName : errors.keySet()) {

                            String errorsForFieldName = errors.get(fieldName).toString();
                            //delete '[' from the beginning of string and ']' from the end of string before storing the errors in session
                            req.setAttribute(fieldName, errorsForFieldName.substring(1, errorsForFieldName.length() - 1));
                        }
                        req.setAttribute("user", userInSession);
                        req.getRequestDispatcher(VIEW_ADMIN_UPDATE_EMPLOYEE_PATH).forward(req,resp);
                    }
                } else {
                    req.setAttribute("generalError", "There is no user in database with id = " + idUser);
                    req.setAttribute("user", userInSession);
                    req.getRequestDispatcher(VIEW_ADMIN_UPDATE_EMPLOYEE_PATH).forward(req, resp);
                }
            } catch (NumberFormatException e) {
                req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
            } catch (DatabaseException e) {
                e.printStackTrace();
                req.setAttribute("generalError", e.getMessage());
                req.setAttribute("user", userInSession);
                req.getRequestDispatcher(VIEW_ADMIN_UPDATE_EMPLOYEE_PATH).forward(req, resp);
            }
        } else {
            req.getRequestDispatcher(VIEW_404_PATH).forward(req, resp);
        }
    }
}
