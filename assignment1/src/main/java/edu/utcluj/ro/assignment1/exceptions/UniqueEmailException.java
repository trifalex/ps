package edu.utcluj.ro.assignment1.exceptions;

/**
 * Created by Alexandru Trif on 11.11.2016.
 */
public class UniqueEmailException extends DatabaseException {

    public UniqueEmailException() {
        super();
    }

    public UniqueEmailException(String message) {
        super(message);
    }
}
