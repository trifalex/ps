package edu.utcluj.ro.assignment1.util;

import edu.utcluj.ro.assignment1.dao.AccountDAO;
import edu.utcluj.ro.assignment1.dao.ClientDAO;
import edu.utcluj.ro.assignment1.dao.ClientUpdateDAO;
import edu.utcluj.ro.assignment1.dao.UserDAO;
import edu.utcluj.ro.assignment1.model.User;

/**
 * Created by Alexandru Trif on 11.04.2017.
 */
public class DaoUtil {

    private static UserDAO userDAO = null;
    private static ClientDAO clientDAO = null;
    private static ClientUpdateDAO clientUpdateDAO = null;
    private static AccountDAO accountDAO = null;

    public static UserDAO getUserDAO () {

        if(userDAO != null) {
            return userDAO;
        } else {
            return userDAO = new UserDAO();
        }
    }

    public static ClientDAO getClientDAO () {

        if(clientDAO != null) {
            return clientDAO;
        } else {
            return clientDAO = new ClientDAO();
        }
    }

    public static ClientUpdateDAO getClientUpdateDAO () {

        if(clientUpdateDAO != null) {
            return clientUpdateDAO;
        } else {
            return clientUpdateDAO = new ClientUpdateDAO();
        }
    }

    public static AccountDAO getAccountDAO () {

        if(accountDAO != null) {
            return accountDAO;
        } else {
            return accountDAO = new AccountDAO();
        }
    }
}
